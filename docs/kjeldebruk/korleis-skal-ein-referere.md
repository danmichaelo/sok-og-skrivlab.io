---
title: "Korleis skal ein referere?"
date: "2012-05-15"
tags: 
  - "kildebruk"
  - "henvisning"
  - "plagiat"
  - "fusk"
---

 # Korleis skal ein referere? 

Korleis kjeldetilvisingar og referanseliste skal sjå ut, vil avhenge av referansestilen du nyttar. I nokre fag står ein fritt til å velje, medan andre har ein bestemt referansestil dei skal følgje. Kva stil som nyttast innan di studieretning vil du finne i studieplanar, få vite av faglærar eller ved å sjå på tidlegare studentoppgåver. Når du har valgt ein referansestil, er det viktig at du føl denne konsekvent. Ver også konsekvent når det gjeld engelsk vs. norsk rettskriving, oppsett av dato mv. 

På Søk & Skriv viser me stilane [APA, 7. utgåve](/referansestiler/apa-7th.html), [Chicago med fotnoter](/referansestiler/chicago-fotnoter.html), [Chicago forfatter-år](/referansestiler/chicago-forfatter-aar.html), [Harvard](/referansestiler/harvard.html), [MLA](/referansestiler/mla.html) og [Vancouver](/referansestiler/vancouver.html).


## Kva skal refererast?

For alt fagleg innhald som ikkje byggjer på eige materiale, eigne resonnement eller meiningar, **skal** ein oppgi kjelde i teksten og i referanselista. Dette kan for eksempel vere andre sine argument, meiningar og vurderingar, talmateriale, modellar, resultat og konklusjonar. Hugs at all bruk av figurar, tabellar, lyd og bilete er knytt til [opphavsrett](https://sokogskriv.no/kjeldebruk/korleis-skal-ein-referere.html#opphavsrett).

**Allmenne sanningar**

Allmenne sanningar treng ikkje referanse, som for eksempel:

> Den 17. mai 1814 skreiv samtlege representantar på Eidsvoll under på ei ny grunnlov for den sjølvstendige staten Noreg, og dei tok Christian Frederik til konge.

Skriv du derimot om noko som ikkje er allment kjend, må du oppgje kvar du hentar opplysningane frå.

Det kan vere vanskeleg å vurdere kor langt dokumentasjonskravet skal gå. Ukontroversielle påstandar som er kunnskapsgrunnlaget innanfor ditt fag, treng ikkje dokumentasjon. Er du i tvil, spør rettleiar. Då unngår du å bli mistenkt for å [plagiere](https://sokogskriv.no/kjeldebruk/korleis-skal-ein-referere.html#korleis-unnga-plagiat-brot-pa-opphavsretten).

**Førelesingar**

Dine eigne førelesingsnotat reknast ikkje som kjelde, og skal ikkje visast til i ei oppgåve. Du kan sjølvsagt bruke førelesingsnotatene dine til inspirasjon når du skriv, men du må arbeide med stoffet, skrive det om og gjere det til ditt eige.

Dersom førelesaren har gjort førelesningsnotat/handouts/powerpoint-presentasjonar offentleg elektronisk, _kan_ du sitere desse. Som hovudregel er det betre å vise til (pensum-)litteratur. Les nøye: har førelesaren oppgitt sine kjelder? I så fall bør du nytte desse i staden for førelesningsnotat/handout/powerpoint-presentasjonen.

## Opphavsrett 
Opphavsrett er den retten skaparen av eit åndsverk har til verket. Åndsverket kan vere eit litterært, vitskapeleg eller kunstnarisk verk, og skaparen blir kalla opphavar. Opphavsretten er regulert i lov om opphavsrett til åndsverk frå 2018, også kalla [åndsverklova](lovdata.no/dokument/NL/lov/2018-06-15-40). Hovudregelen i åndsverklova er at opphavaren har einerett til å framstille eksemplar av verket og å gjere det tilgjengeleg for andre. Denne økonomiske retten varer i 70 år etter at opphavar er gått bort, då «fell verket i det fri», som det heiter. 

I tillegg til den økonomiske råderetten opphavar har, kjem den ideelle – det vil seie retten til å bli namngjeven og verna mot at åndsverket blir nytta på eit krenkande vis. Merk at den ideelle retten ikkje går ut på dato. 

## Korleis unngå plagiat / brot på opphavsretten? 

::: warning Korleis kan du unngå å plagiere?

Det viktigaste er å **aldri klippe og lime utan å oppgje kjelde**. 

Gjengi sitat ordrett med "sittateikn", **eller** parafraser (gjengi innholdet med eigne ord). 

Legg vekk kjelda medan du skriv, så unngår du å bli opphengd i skrivemåten til forfattaren. Sjekk at meiningsinnhaldet stemmer, og vis til kjelda som vanleg. 

Vips, så har du unngått å plagiere!
  
:::  

Brot på gjeldande reglar om opphavsrett kan få uheldige følgjer. _Plagiat_ er å framstille andre sine resultat, tankar, idear eller formuleringar som om dei var sine eigne. Dette reknast som intellektuelt tjuveri ifølgje [åndsverksloven](https://www.lovdata.no/all/hl-19610512-002.html). 

Omfattande og/eller medvite plagiat reknast som fusk, og vil få uheldige følgjer for deg som student. Dersom du blir skulda for å plagiere – ved å ha kopiert eit verk heilt eller delvis utan å vise til kor du har det frå – kan det forsinke studieprogresjon og i verste fall føre til tap av studieplass.  
Vis alltid kvar du har henta informasjon eller formuleringar frå – då er du trygg. 

::: warning Sjøvplagiat 

Dette gjeld òg for ditt eige arbeid: _Sjølvplagiat_ vert nemleg også rekna som plagiat. 

::: 

For meir om opphavsrett, sjå [DelRett](https://www.delrett.no/nb) som er ei offentleg rettleiingsteneste om opphavsrett. 


## Sitat
Nokon gonger kan det vere aktuelt å hente informasjon eller tekst frå andre inn i eigen tekst. Dette må gjerast i samsvar med god skikk, ved å sitere og vise korrekt til kjeldene. Ikkje gje inntrykk av at noko er ditt når det ikkje er det.

### Direkte sitat

Direkte sitat er ei heilt ordrett attgiving. Døme kan vere definisjonar, særleg gode formuleringar eller utsegn til vidare drøfting. Sitat som er mindre enn 40 ord (eller tre linjer) skrivast direkte inn i teksten og uthevast med hermeteikn. 

::: eksempel Eksempel
«Studenter (og forskere) innenfor samfunnsfag og humanistiske fag må skrive innenfor en akademisk sjanger. Særtrekket ved denne sjangeren er _drøfting_» (Førland, 1996, s. 11).

Kjelde: Førland, T.E. (1996). _Drøft! Lærebok i oppgaveskriving_. Gyldendal.
:::

Sitat som har meir enn 40 ord skal (i APA) skrivast i eit eige avsnitt med innrykk. Når du skriv sitata på denne måten skal det ikkje nyttast hermeteikn. Det er også vanleg å introdusere sitat med innleiing og/eller ein kommentar. 

### Ved endringar i sitatet 

På same måte som med indirekte sitat, kan det nokre gonger vere aktuelt å gjere endringar på sitatet. Det kan vere å ta vekk delar av sitatet, eller å legge til ord for å få lesaren til å forstå samanhengen. Om du har eit lengre sitat med ein del irrelevante passasjar kan det vere aktuelt å **utelate** delar av sitatet. Dette må markerast tydeleg. Om du utelét eitt eller to ord kan det markerast med ... (tre prikkar). Dersom du utelét fleire ord, kan du nytta skarpe klammer \[ ... \] eller vanlege parentesar ( ... ). 


::: eksempel Eksempel

"Studenter ... må skrive innenfor en akademisk sjanger. Særtrekket ved denne sjangeren er _drøfting_" (Førland, 1996, s. 11).

:::

Dersom du ønskjer å **tilføye** eller **erstatte** noko i eit sitat, markerast dette ved å nytte klammeparentesar. 

::: eksempel Eksempel

"Studenter (og forskere) innenfor \[de akademiske disipliner\] må skrive innenfor en akademisk sjanger" (Førland, 1996, s. 11).

:::

### Indirekte sitat

Indirekte sitat, også kalla parafrase, syner til dei tilfella der du gjev att innhaldet med dine eigne ord. Indirekte sitat kan bidra til å skape flyt i teksten og kan synleggjere at du har ei god forståing for kjelda. Pass på at innhaldet blir gjeve att korrekt og at meininga framleis er den same.

::: eksempel Eksempel
Førland (1996, s. 11) hevder at alle studenter innenfor samfunnsfag og humaniora må lære seg den akademiske sjangeren, som kjennetegnes av drøfting.

Den akademiske sjangeren kjennetegnes av drøfting, og alle studenter innenfor samfunnsfag og humaniora må lære seg den (Førland, 1996, s. 11)
:::

### Sekundærreferansar

Hovudregelen er at du kun skal vise til verk du har lese. Om originalkjelda ikkje er tilgjengeleg, eller er på språk du ikkje forstår, kan du vise til andre si omtale av kjelda:

::: eksempel Eksempel

"Det er ikke tilfeldig at det moderne menneske skriver 'med' en maskin" (Martin Heidegger, i Johansen, 2003, s. 80).

Kjelde: Johansen, A. (2003). _Samtalens tynne tråd: skriveerfaringer._ Spartacus. 

:::

## Notar og vedlegg

- Avgrens bruk av notar.
- Notar skal nyttast til tilleggsopplysningar som ikkje er ein naturleg del av teksten. 
- Du kan velje om du vil nytte fotnotar nedst på sida eller som sluttnotar bak i kapitlet/oppgåva. Vel du å plassere notane som fotnotar er det vanleg å ha ein mindre skrifttype for å skilje notane frå hovudteksten.
- Nøyaktig korleis notetilvisinga gjerast er ofte avhenging av kva referansestil du nytter. Det er for eksempel ikkje føremålsteneleg å nytte nummererte notar dersom du nytter ein nummerert referansestil som Vancouver. 
- Vedlegg er lister over tabellar og figurar som er med i oppgåva, spørreskjema, observasjonsskjema, intervjuguide og liknande. Vedlegg skal nummerast og plasserast etter referanselista.


## Hjelpemiddel

 
For større oppgåver kan du effektivisere arbeidet ved å nytte referanseverktøy som EndNote, ReferenceManager, Zotero eller Mendeley. 
Undersøk med studiestaden din kva referanseverktøy du har tilgong til. [Zotero](https://www.zotero.org/) er gratis tilgjengelege for alle, medan [Mendeley](https://www.mendeley.com/) og [EndNote](https://endnote.com/product-details/basic/) finst i enklare gratisversjonar. 

Hugs at du alltid må dobbelsjekke at referansen blir rett! 

::: tip Tips: 
Det er fullt mogeleg å skrive ei oppgåve eller ein artikkel utan å bruke eit referansehandteringsverktøy. Referansar kan enkelt kopierast frå Oria, Google Scholar eller frå heimesida til tidsskriftet. Pass på å sjekke at dei er fullstendige og at eventuell kursiv føl med. Sjå etter sitatteikn:

<List
  image="/images/scholar-cite.svg"
  alt="Scholar cite"
  text="Knapp for Google Scholar siteringsfunksjon."
/>
:::


## QUIZ: Kva type kjelde er dette?

<QuizEn v-bind:quizNum=1 />

