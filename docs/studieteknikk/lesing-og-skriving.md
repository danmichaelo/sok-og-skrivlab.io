---
title: "Lesing og skriving"
date: "2012-12-07"
tags:
  - oppgaveskriving
  - gjenbruk
  - stikkord
---

# Lesing og skriving

Oppgaveskriving i høyere utdanning går ut på å bruke det du har lest og lært til å skrive noe selv, på en selvstendig måte. Du former din egen tekst blant annet gjennom å omforme og diskutere med andres tekster.

Gjennom lesingen plukker du opp og samler inn momenter du kan bruke i skrivingen. Husk at du da endrer på den opprinnelige sammenhengen for det materialet du bruker. Når du viser til eller siterer fra en annen tekst, bruker du den til å forme egne resonnementer. Alt som skjer i din tekst er det du som gjør. Samtidig kan du ikke bare overta det andre har tenkt og skrevet som om det var deg selv som hadde tenkt og skrevet det. Du må skille klart mellom egne resonnementer, fortolkninger, parafrasering (indirekte sitater), og direkte sitater. Du må altså beherske grunnleggende [sitat- og referanseteknikk](/kjeldebruk/korleis-skal-ein-referere.html).

::: eksempel Hvordan kan jeg skrive selvstendig? 
Hør filosof og skrivementor Åsne Grøgaard gi en innføring i hva det vil si å «ta stilling» til noe du har lest. 

<Video id="FJKg3G-JRpg" />
::: 

## Hvordan ta gode notater? 

Mens man leser en tekst, hender det at man tenker: «Det var interessant, det visste jeg ikke, det må jeg huske på». Hvordan kan du ta med deg disse glimtene av innsikt videre i studiet? Her skal vi si litt mer om å ta notater og om andre måter du kan kombinere lesing og skriving i arbeidet ditt.

Å skrive stikkord, markere og streke under i teksten er en teknikk du kan bruke for å bli en mer aktiv leser. Men vær klar over at en tekst full av understrekninger kan gi et falskt inntrykk av at du har jobbet grundigere med teksten enn du faktisk har. Å streke under er lett; å trekke ut viktig informasjon og viktige poenger og ordne stoffet i egne notater er vanskeligere – men til gjengjeld gir det større utbytte. 

::: tip Skriv notater til det du leser i et eget dokument
Dette vil automatisk få deg til å skrive bedre og mer informative notater enn hvis du bare noterer i selve teksten. Lag gjerne korte sammendrag av det du leser.
::: 

Få tak i tekstens hovedbudskap, argumentasjon og sammenhengen teksten står i. Vent gjerne med understrekning og notering til du har lest gjennom teksten, eller til du har lest nok til at du skjønner hvilken retning teksten tar og hva den dreier seg om. (Mer om dette under [Lesemåter](/studieteknikk/lesemater.html)). Hvis du fester deg for mye ved detaljer, kan du oppleve at det blir vanskeligere å gripe helheten i teksten. Legg merke til  steder i teksten der forfatteren samler sine argumenter eller der hvor hovedpoengene kommer tydeligst til uttrykk.

Bli enig med deg selv om hvilke tegn du bruker til å markere hva med. Streker, dobbeltstreker, ringer rundt bestemte begreper, utropstegn eller kryss er eksempler på tegn man kan bruke til å markere i teksten. Når du tar frem teksten igjen, går det raskere å få øye på det viktigste.

::: tip Tenk på:
Hva du oppfatter som det viktigste, kan forandre seg. Første gang du jobber med en vanskelig tekst om et nytt emne, kan det hende at du gjetter ganske mye hva som er viktig i teksten. Det er ikke sikkert at du gjetter riktig i første omgang. Vær åpen for at du kan ha oversett noe viktig, kanskje til og med det viktigste.
:::

I løpet av en skriveprosess kan du få bruk for å gå tilbake til og lese en tekst på nytt, fordi ditt perspektiv kan ha endret seg. 
+ Kanskje forstår du argumentasjonen bedre etter å ha lest noe annet? 
+ Kanskje prosessen med å drøfte har fått deg til å stille andre spørsmål? 
+ Kanskje kan du ha nytte av teksten på en annen måte enn du først trodde fordi du har endret problemstillingen din?


## Å skrive sammendrag

Sammendrag er et veldig godt verktøy for å sette seg inn i og forstå et stoff. Når du skriver sammendrag, bør du konsentrere deg om å få fram hovedtrekk ved tekstens formål, argumentasjon og struktur. [Sammendraget skal være lojalt](/kjeldebruk/korleis-skal-ein-referere.html) overfor originalteksten. Det gjelder å fremstille problematikken og argumentasjonen i en tekst på dens egne premisser. Poenget med å skrive sammendrag er _ikke_ å kritisere synspunktene eller å belyse egne problemstillinger. Skriv sammendraget med sikte på å gjengi andres påstander og argumenter – med dine egne ord – på en slik måte at de ville kunne si: «Ja, det var det jeg mente».

Å skrive sammendrag kan være krevende, men svært nyttig når du skal skrive større oppgaver, rapporter, i undervisning og faglige diskusjoner. 

## Les og del 

Når du skriver en oppgave, er det viktig å lese gjennom den flere ganger. Vær kritisk, og still de samme spørsmålene til din egen tekst som du stiller til andres: Hva vil jeg fram til her? Gjør jeg klart rede for problemstillingen? Besvarer jeg den i løpet av oppgaven?

Akkurat som med andre tekster, er det lurt å legge den fra seg en stund for så å komme tilbake og se på den med friske øyne. Er noe uklart? Kommer det tydelig fram hva jeg vil undersøke, hva jeg vil demonstrere eller argumentere for, hva som er mine poenger og hva som er andres? 

::: oppgave Les teksten din høyt for deg selv 
Dette er en god måte å oppdage eventuelle problemer som dine lesere vil kunne støte på. Er setningene for lange? Er det mange gjentakelser? Det er lett å se seg blind på sin egen tekst, men du kan ofte _høre_ at noe kan bli bedre.
:::

La også andre lese og kommentere teksten din, gjerne både medstudenter og andre som ikke er like innforstått med tematikk og terminologi. De kan se ting ikke du ser. Når dere leser hverandres tekster, kan dere bli inspirert, men dere kan også bli oppmerksomme på svakheter og feil dere selv vil unngå. Det kan altså være nyttig både for den som leser og kommenterer og for den som får teksten sin kommentert.
