---
title: "Studieteknikk"
date: "2012-03-15"
prev: "../soking/systematisk-soking"
tags:
  - lesing
  - studieteknikk
  - mestring
  - studiemestring

---

# ![](/images/illustrasjoner_lesing_500x450.png) Studieteknikk

Å være bevisst egen studieteknikk er til stor hjelp for deg som skal studere. Når vi skriver om studieteknikk, vil vi konsentrere oss om lesing, lesestrategier og skrivestrategier. Her får du tips om hvordan du kan lese og forstå tekster, ta gode notater, samarbeide og jobbe i studiegrupper.

::: tip Studiemestring 
Studieteknikk handler også om livskvalitet, mestring, kognitiv trening og gode rutiner. Nettressursen [Eksamenskoden](https://eksamenskoden.no) dekker mye av dette, og vi anbefaler en tur innom. 
::: 


# Lesing

I det akademiske håndverket er lesing og skriving tett forbundet. For å kunne skrive en god oppgave, må du lese effektivt og målrettet. Og omvendt, når du trener deg opp til å lese med oppmerksomhet, vil du også kunne lese dine egne utkast og ideer på en oppmerksom måte. 

Råd nummer én til deg som skal lese i forbindelse med studier, er derfor: Les! Bare slik kan du bli en god leser og skriver. Hør Birger Solheims råd om hvordan du som student kan bli en bedre leser: 



<Video id="JchpFI50UDk" />



