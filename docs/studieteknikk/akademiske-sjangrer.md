---
title: "Akademiske sjangrer"
date: "2012-06-19"
next: "../skriving/"
tags: 
  - sjanger
  - kildetyper 
  - teksttyper
  - tekstforståelse
  - sakprosa
  - sjangerinndeling
  
   
 
---

# Akademiske sjangrer

Når du studerer, leser du ulike typer tekster: lærebøker, oppslagsverk, vitenskapelige og populærvitenskapelige artikler og essays, konferanseinnlegg, rapporter og avhandlinger.

Du leser med andre ord tekster i ulike sjangrer. Å være bevisst om hva slags sjanger du har å gjøre med når du leser en tekst, er en del av tekstforståelsen. Sjanger er også nyttig å tenke på når du skal skrive akademiske tekster selv, og lar deg inspirere av tekster du leser. Skal du for eksempel skrive en semesteroppgave (bacheloroppgave) eller en hjemmeeksamen, skal teksten du skriver ligne mer på en artikkel enn på et kapittel i en lærebok. Det betyr at du skal argumentere og problematisere - drøfte - og ikke bare presentere kunnskap.

På skolen lærer man om både skjønnlitterære sjangrer (dikt, noveller, romaner, skuespill) og om sakprosasjangrer. Innenfor sakprosa finner vi både akademiske og ikke-akademiske sjangrer. Eksempler på det siste er journalistiske sjangrer som lederartikkel og nyhetsreportasje. Her skal vi kort omtale noen akademiske sjangrer og hva som kjennetegner dem.


### Lærebok

Formålet med en lærebok er å formidle etablert kunnskap. Målgruppen er studenter i faget. En lærebok gir en innføring i hva faget dreier seg om, hva man vet i faget og hvilke problemstillinger, begreper og teorier faget består av. En lærebok presenterer kunnskap, og den forklarer hva kunnskapen har vært brukt og kan brukes til. Den informerer og instruerer. Selv om en lærebok også kan inneholde drøfting og argumentasjon, er dens viktigste oppgave ikke å argumentere for bestemte oppfatninger, men å redegjøre for de oppfatninger som allerede finnes i faget.

### Vitenskapelig artikkel

Formålet med en vitenskapelig artikkel er å presentere ny kunnskap eller gi nye perspektiver på et faglig spørsmål. Målgruppen er andre fagpersoner. En vitenskapelig artikkel er først og fremst argumenterende: Den hevder at noe er tilfelle eller sannsynligvis tilfelle, og underbygger påstanden med evidens (argumenter, forskningsresultater). En vitenskapelig artikkel må derfor både ha gyldig argumentasjon og grundig dokumentasjon. Dessuten må forfatteren av en vitenskapelig artikkel anvende anerkjente metoder og gjøre rede for disse i artikkelen.

### Avhandling

En avhandling er et større, vitenskapelig arbeid. I mange fag er det vanlig å sette sammen en avhandling av (allerede publiserte) artikler, med en innledning (kappe) som forklarer hvordan artiklene inngår i avhandlingens overordnede sammenheng. I andre fag (spesielt humanistiske fag) er det vanligere å skrive en monografi, altså ett sammenhengende arbeid som er inndelt i kapitler. Avhandlingen har fellestrekk med artikkelen: Den presenterer ny kunnskap overfor fagfolk, den argumenterer, den har grundig dokumentasjon og den gjør rede for metoden som er brukt. Når du skal skrive masteroppgave, er det en avhandling du skal skrive.

### Populærvitenskapelig framstilling

Formålet med populærvitenskapelige framstillinger, i enten bøker eller artikler, er å formidle etablert kunnskap til «den alminnelige leser». Ettersom formålet normalt ikke er å etablere ny kunnskap eller utfordre etablerte sannheter, vil innslaget av argumentasjon være mindre enn i de vitenskapelige sjangrene. Isteden legges det vekt på å levendegjøre stoffet ved hjelp av fortellinger, anekdoter, illustrasjoner og lignende. Av og til kan teksten i sin helhet være bygget opp som en fortelling, for eksempel en biografi om en betydningsfull forsker eller en framstilling av et historisk forløp.

### Leksikonartikkel

Formålet med en leksikonartikkel er å presentere etablert kunnskap på en nøytral, kortfattet og oversiktlig måte. Målgruppen er gjerne den alminnelige leser så vel som fagfolk. Det finnes imidlertid også vitenskapelige leksika og andre oppslagsverker som forutsetter fagkunnskaper hos leseren. Leksikonartikkelen tar uansett normalt ikke sikte på å overbevise noen om noe ved å argumentere for det. Den forklarer og instruerer heller ikke slik som en lærebok gjør det. Leksikonartikkelen er først og fremst informativ og deskriptiv. Den gir opplysninger, definerer ord og begreper og beskriver saksforhold.

Se også [kildevurderinger](/kjeldebruk/kjeldevurdering.html) og [målgruppe og sjanger](/kjeldebruk/kjeldevurdering.html#malgruppe-og-sjanger).

## Sjanger og språklige handlinger 

Etter denne korte omtalen av noen akademiske sjangrer, er det verdt å merke seg følgende:

- En sjangerinndeling er en grovsortering ut fra tekstegenskaper og kommunikasjonssituasjoner, men en sjangerinndeling er også selv en sjanger: en måte å inndele (klassifisere) tekster på. 
- Det er ikke vanntette skott mellom sjangrene. Ofte vil vi finne sjanger*blanding* eller sjanger*brudd* i de tekstene vi leser. En biografi kan befinne seg på grensen mellom populærvitenskapelig og vitenskapelig, og av og til vil det være mest dekkende å snakke om en vitenskapelig biografi. En faglig artikkel kan være mer spørrende og problematiserende, mer personlig og språklig original enn en standard vitenskapelig artikkel. Da vil vi kanskje heller omtale artikkelen som et essay. Her er det variasjoner både i terminologi og mellom ulike faglige tradisjoner.

I omtalen ovenfor deler vi inn sjangrene etter _formål_ og _målgruppe_. I tillegg bruker vi en del ord (verb) for å si noe om hva som skjer i de ulike sjangrene, eller hva tekster i de ulike sjangrene vanligvis _gjør,_ som «formidle», «forklare», «presentere», «argumentere», «informere», «beskrive», «fortelle». Vi kan si at en sjanger er kjennetegnet av at en eller noen slike språkhandlinger dominerer over andre. En leksikonartikkel er først og fremst informerende. En artikkel er først og fremst argumenterende. Det betyr ikke at en artikkel ikke også er informerende, men informasjonen som gis er underordnet argumentasjonen.

Når vi leser akademiske tekster, kan vi prøve å legge merke til hvilke _språkhandlinger_ vi finner i teksten. Språkforskere (lingvister) snakker av og til om «ulike teksttyper». En vanlig firedeling (som går tilbake til amerikansk skoleundervisning i retorikk og skriveøvelser fra 1800-tallet), er denne: Exposition, Description, Narration, Argumentation, forkortet EDNA. På norsk: Presentasjon (forklaring, redegjørelse), Beskrivelse, Fortelling, Argumentasjon.

I tillegg til å snakke om at akademiske tekster inngår i ulike akademiske sjangrer, kan vi dermed si at akademiske tekster forklarer, beskriver, forteller og argumenterer (drøfter, problematiserer). Andre inndelinger er selvsagt også mulige. 

::: oppgave Hva gjør teksten?
Ett av spørsmålene du kan stille til en tekst, er: Hva gjør denne teksten nå? Forklarer den? Beskriver den? Forteller den? Argumenterer den? Hva mer gjør den? 

Ved å spørre slik, kan du skjerpe din forståelse av hva som foregår i de tekstene du leser. Da vil lesingen også fungere som en aktiv forberedelse til din egen skriving.
:::
