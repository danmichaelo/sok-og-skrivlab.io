---
title: "Lesemåter"
date: "2012-09-14"
tags: 
  - lesing
  - lesetips
  - notater 
  - analyse
---

# Lesemåter

Det finnes ulike måter å lese på. Hvordan vi leser avhenger av _hva_ vi leser, men også av formålet med lesingen. Vi leser en roman på en annen måte enn en innføring i statistikk, men vi leser også en roman på en annen måte dersom vi skal lage en litterær analyse av den, enn hvis vi "bare leser" den.

Når du leser en roman som underholdning, lever du deg inn i fortellingen, blir kjent med personene, og er spent på hvordan det vil gå dem. Hvis du skal analysere en roman leser du målrettet ut fra bestemte spørsmål til teksten: Hvordan er fortellingen bygget opp? Hvordan blir personene introdusert og beskrevet? Hva slags språklige virkemidler tas i bruk? Hvilken sammenheng står romanen i?

## Hva er målet for lesingen?

Hvis målet ditt er å skaffe deg generell kunnskap om et nytt fag eller tema, så leser du i bredden mer enn i dybden. Du begynner kanskje med å google, studere _Wikipedia_, orientere deg i referanselister eller siteringer og gå på biblioteket.

Hvis målet er å bruke det du leser for å skrive en oppgave, må du lese mer fokusert. Du ser etter stoff som har  direkte relevans for oppgavens tema og problemstilling. Du spør: Kan jeg bruke det jeg leser nå, til å skrive denne oppgaven? Du leser i dybden med en bevisst hensikt, og går i aktiv dialog med det du leser.

::: tip Aktiv lesing 
Det er uansett viktig å forholde seg aktivt til tekstene du leser. Å studere dreier seg både om å tilegne seg kunnskap og å anvende den.
::: 

Ikke alle tekster er like gode, og ikke alle tekster er relevante for deg. Mer om dette finner du under [kildevurdering](/kjeldebruk/kjeldevurdering.html). En viktig del av studiene dreier seg om å øve seg i å forholde seg _kritisk_ og _selvstendig,_ ikke bare til det teksten handler om, men også til teksten selv. Når du leser pensum, kan det være nyttig å spørre seg: Hvorfor har vi denne teksten på pensum? Hva er det som er viktig med denne teksten, siden den er havnet på pensumlisten? Hva er forbindelsen mellom denne teksten, emnebeskrivelsen og resten av pensumlitteraturen?

::: tip Varier lesingen 
Det fins ingen fasit for hvordan en får best mulig utbytte av en tekst, og lesere er forskjellige. Men generelle råd fins: Øv deg i å bli en aktiv leser ved å variere måten du leser på. En anbefalt fremgangsmåte er å lese samme tekst i flere omganger, på ulike måter. 
::: 

## Å lese en tekst i flere omganger

- **_Orienter deg i teksten_**. Få et første overblikk, finn ut hva slags tekst det er og hva den dreier seg om. Studer overskrifter, mellomtitler, sammendrag og referanseliste. Hvis det er en bok, studér innholdsfortegnelse, forord, eventuelt emneregister, referanseliste, baksidetekst og annet som beskriver teksten. Les eventuelt noen avsnitt her og der, for eksempel første og siste avsnitt i en artikkel eller åpningen av kapitlene i en bok. På den måten stiller du bedre forberedt til selve lesingen: Du har allerede en idé om hva som kommer.

- **_Les hurtig gjennom_**. Neste skritt er gjennomlesing. Begynn med å lese gjennom hele teksten _uten_ å ta notater, bortsett fra små merker hvis det er noe du biter deg spesielt merke i.

Hvis det er noe du ikke forstår: Les videre. Hvis du ikke synes du forstår noe som helst: Les likevel. Du kommer tilbake til det både én og to ganger.

- **_Les teksten grundig_**. Denne gangen leser du for å forstå mest mulig. Fordyp deg i teksten. Bruk penn og papir eller elektronisk notatverktøy, og ta notater mens du leser. I løpet av semesteret kan du bygge deg opp et «notat»- eller «pensumarkiv». Et slikt arkiv er gull verdt når du skal repetere før eksamen eller skrive oppgaver. 

Når du har orientert deg i teksten, lest raskt gjennom, og deretter jobbet grundig med å lese og notere, kan du legge teksten til side. Når du så vender tilbake til teksten, for eksempel uka etter, kan du lese gjennom den **raskt**. Antakelig vil oppfatte teksten litt annerledes nå. 

## Spør teksten

Still spørsmål til teksten, og forsøk å «tvinge» den til å svare. Når du leser en tekst med tanke på å få den til å kaste lys over noe du lurer på eller ikke forstår, vil lesingen bli både lettere og mer interessant. Spørsmålene kan variere med lesingens formål: alt fra spørsmål om tekstens agenda og hovedbudskap, hvordan argumentasjonen fungerer, til spesifikke spørsmål om ord og begreper. Det er ikke teksten som skal tenke for deg, men du som skal tenke ved hjelp av teksten.

::: oppgave Oppgave

Ta for deg en tekst du har på pensum, og spør: Hva er temaet? Hva er problemstillingen? Hva undersøker forfatteren, hva kommer hun fram til, og hvordan kommer hun fram til det? Kan du bruke noe av det i din oppgave, og i så fall, hvordan vil du selv bruke det?
:::

## Se på konteksten

Å se etter sammenhengen teksten inngår i, kan hjelpe deg til å få et bedre grep om teksten og føre deg videre inn i faget. Kontekstualiserende lesing kan sammenlignes med orienterende lesing; forskjellen er at du nå ikke bare orienterer deg i selve teksten, men også i tekstens «omland». Du ser etter slike ting som:

- **Forfatteren** av teksten. Hvem er det? Et søk på forfatternavn vil i de fleste tilfeller gi napp. Du finner nettsider, leksikonartikler eller andre publikasjoner. Tilhører forfatteren\[e\] en bestemt **teoretisk retning**? Kan du i så fall finne ut noe mer om hva som kjennetegner den?
- Forfattere viser til **andre forskere**. Hva kan du finne ut om dem?
- Hva er sentrale **begreper** i teksten?
- Er teksten omtalt eller referert til andre steder, for eksempel i andre **bøker og artikler**? Kanskje det finnes **anmeldelser**?

::: eksempel Eksempel 
Du leser et avsnitt der det står:

«Syden kan altså klart sies å være et inversjonsritual i Gluckmanns forstand og en heterotop i Foucaults forstand ... » (Runar Døving, «Stedet Syden», _Norsk Antropologisk Tidsskrift_ 1/2011, s. 25).
:::

Ordet «altså» tyder på at det forut for dette avsnittet er sagt noe om både «inversjonsritual i Gluckmanns forstand» og «heterotop i Foucaults forstand» - og kanskje det som er sagt er nok for å forstå hva det snakkes om i dette avsnittet. Likevel: Hvem er Gluckmann og hvem er Foucault? Det er ikke sikkert det er sagt så mye om dem. Kanskje de er berømte forskere, som det hører med til fagkompetansen å kjenne til? Og hva betyr «inversjonsritual» og «heterotop»? Kanskje forfatteren forklarer det greit, men sannsynligheten er stor for at det ikke er bortkastet å søke hjelp også andre steder.

Det vi leser i akademisk sammenheng skal altså brukes til noe, og derfor må vi lese med en annen oppmerksomhet enn når vi leser for underholdningens skyld. Å være en god leser innebærer også at du blir bedre til å skrive. Når du leser andres akademiske tekster, lærer du mye av å se på struktur, setningsoppbygging og argumentasjon. Det er nærmest umulig å skrive en akademisk tekst om du aldri har lest en!


