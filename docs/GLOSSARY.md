---
terms:
    APA: "American Psychological Association"
    MLA: "Modern Language Association"
    lærebok: "er en type tekst skrevet for studenter som skal innlemmes i en faglig diskurs og et nytt fag"
---
 
# Glossary
 
<Glossary :terms="$frontmatter.terms" />
