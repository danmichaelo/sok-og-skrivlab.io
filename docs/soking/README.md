---
title: "Søking"
date: "2012-06-28"
---

# ![](/images/illustrasjoner_sok_500x450.png) Søking

I denne delen finner du tips om hvordan du søker etter informasjon og hvordan informasjonssøk kan hjelpe deg i gang med oppgaveskriving. Faglig informasjonssøking er en prosess som krever tid og planlegging. Den nye informasjonen sammenholdes med kunnskapen vi har fra før. Dette utvider vår kunnskap og bidrar til å skape ny mening og forståelse. Behovet for informasjon kan utvikle seg, fra en vag forestilling om noe du trenger kunnskap om, til det du trenger for å besvare en spesifikk problemstilling.
Før du går i gang med å søke, tenk over:
*   Hvilken type informasjon trenger du for å svare på problemstillingen?
*	Hvordan kan du finne denne informasjonen?
*	Hvordan kan du vite at du har funnet tilstrekkelig og relevant informasjon?

I løpet av oppgaveskrivingen vil du ha bruk for forskjellige typer kilder, og kriteriene for å vurdere informasjon kan variere. På disse sidene introduseres noen verktøy og innganger som kan være nyttige i de ulike fasene.

