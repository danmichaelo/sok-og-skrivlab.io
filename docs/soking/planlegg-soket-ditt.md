---
title: "Planlegg søket ditt"
date: "2012-04-19"
---

# Planlegg søket ditt 

I en tidlig fase kan det være nødvendig å orientere seg bredt for å avklare problemstilling, metode og tilnærming. Da kan det være nyttig å skumlese ulike kilder. Noen av kildene vil du ta med deg i det videre arbeidet, mens andre bare er nyttige i den innledende fasen.

<Video id="KFbQV7If_ZY" />

## Finn bakgrunnsinformasjon

*	Generelle oppslagsverk som _Wikipedia_ og _Store norske leksikon_ dekker et vidt spekter av emner og gir pekere til mer dyptgående kilder.
*	Fagspesifikke oppslagsverk (for eksempel _International Encyclopedia of the Social & Behavioral Sciences_) gir grundige innføringer. Forfatterne er eksperter på sine felt og har kartlagt den sentrale litteraturen i oversiktsartikler.
*	Lærebøker fra pensum- og ressurslister gir innføring i og henvisninger til kilder som går mer i dybden.
*	Gjennom nyhetsarkivet _Atekst_ og Nasjonalbibliotekets digitale avistjeneste får du tilgang til den norske samfunnsdebatten. Begge arkivene er tilgjengelige i de fleste bibliotek i Norge.
*	Offentlig informasjon, som utredninger, stortingsmeldinger og statistikk, ligger lett tilgjengelig på nettet, se for eksempel www.regjeringen.no, _Statistisk sentralbyrå_,  _Verdensbanken_ og OECD.


## Finn faglitteratur

Når du har lest deg opp på emnet, og problemstillingen begynner å ta form, kreves det informasjon som går mer i dybden. Sensor og faglærer forventer at du bruker vitenskapelige kilder som grunnlag for oppgaven. I tillegg til fagbøker, er artikler i tidsskrifter med fagfellevurdering den viktigste inngangen til vitenskapelige tekster. At et tidsskrift er fagfellevurdert (engelsk: _peer reviewed_) vil si at artiklene blir vurdert og godkjent av andre forskere før publisering.

## Valg av databaser

Gjennom bibliotekenes nettsider (UiO, UiB, HVL) får du tilgang til databaser som dekker et bredt spekter av fagområder. En database er et elektronisk arkiv som inneholder ulike typer kilder. Noen databaser er tverrfaglige, mens andre dekker avgrensede fagområder. De fagspesifikke databasene gir bedre dekning av litteraturen på fagområdet enn de mer generelle databasene.

Gjør deg kjent med de databasene som er aktuelle for ditt emne. Husk at ingen databaser dekker alt; de overlapper og utfyller hverandre. Derfor er det viktig å bruke flere databaser for å få oversikt.
Nedenfor finner du et utvalg tverrfaglige databaser som kan være et godt utgangspunkt for søk før du går videre til de fagspesifikke databasene:
*	Oria er forskningsbibliotekenes søkeverktøy. Her finner du blant annet fagbøker, masteroppgaver, avhandlinger og tidsskriftsartikler.
*	_Google Scholar_ er den akademiske versjonen av _Google_. Den søker etter vitenskapelig litteratur fra anerkjente forlag og forskningsdatabaser.
*	Artikkelbasen Norart gir oversikt over norske og et utvalg nordiske tidsskriftsartikler. Basen dekker både populærvitenskapelige og vitenskapelige tidsskrifter, så her må du selv foreta en kritisk vurdering.
*	_Idunn_ dekker tidsskrifter som er utgitt på Universitetsforlaget. Tilgjengelig i de fleste bibliotek i Norge.
*	Publiseringsarkivet NORA gir deg oversikt over forskningsaktiviteten i helse- og instituttsektoren og universitets- og høgskolesektoren. Søk i NORA eller eget institusjonsarkiv (BORA, DUO, HVL Open) for å finne  masteroppgaver og doktoravhandlinger. 


::: tip Tips 
For å unngå tilfeldig og usystematisk leting etter litteratur, kan det være lurt å sette opp en plan for søket. Det kan spare tid og sikre at alle viktige elementer i problemstillingen blir tatt med. Beskriv hvilke søkeord du har brukt, og hvordan disse er kombinert. 
:::


