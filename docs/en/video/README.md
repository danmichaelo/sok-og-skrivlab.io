---
title: "Videos"
sidebar: false
---

# Videos


## How to search with keywords

<Video id="J2LCsJlOEsI" />



## Search with AND / OR

<Video id="lEo96kOKGmA" />



## Where to find research literature

<Video id="3YUAb9G8uUg" />



## Kajsa & Sandhya’s Writing Club

University of Bergen PhD students Kajsa Parding and Sandhya Tiwari talk about their writing club, and how it has helped them with their thesis work.

<Video id="pbH-PqsYxK8" />



## Saying What You Mean part 1 & 2

A good approach to argumentation in academic writing, part 1 of 2.

<Video id="OWeAPxlxGnE" />

<Video id="DVTg57airZg" />
