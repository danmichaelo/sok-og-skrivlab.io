---
title: "Different sources"
date: "2020-06-25"
---

# Different sources

## What is the purpose of sources? 

Information can be presented in many different ways and in different formats. Writing an assignment is partially about knowing which sources and information will be the most useful for your needs. Each source has its own purpose and role. In many cases, the types of sources we use will be those that help build our reasoning and overall strength of the thesis. Other sources are used for definitions or documentation, such as encyclopedias. The type of sources you use will depend on the information you need for your assignment, and the points you are trying to make. 

## Articles 

Articles come in a variety of formats, e.g. in journals, printed newspapers or online. Always assess the type of article you have in front of you. Many articles (especially those published in scientific journals) have a high level of quality and can help make your arguments stronger.

### The scientific article  

A scientific article is a text where an author writes about a specific subject or case. The article must present new insights or support existing knowledge in a verifiable form. The scientific article is used to disseminate research results within and across the scientific community. 

### Review Article  

A review article provides an overview of a research topic based on a range of studies. The review offers a summary, analysis, knowledge status, and sometimes conclusion to a research question. The review article is a good place to start as a general orientation in a field of research.   


### Professional Peer Review 

Professional peer review means that an article has been reviewed by at least two professionals within a field of research before being accepted for publication. The author(s) of the article receive feedback and suggestions for improvements that must be made before the article can be published. This helps to ensure that articles meet a certain level of quality. This also helps to ensure that the articles follow the academic and scientific guidelines of their specific field.  

Always consider the source for both peer-reviewed articles and articles published in other journals. Articles that are not peer reviewed can still be relevant and good.  


### Essay 

The classical essay is a personalized form of an article that is clearly shaped by the author's opinions and attitudes. The author may use personal experiences, anecdotes, or methaphors as illustrative examples.  It is often written with the intention to entertain and can be presented in a more approachable language. The classical essay falls into a genre of writing between subject and prose based writing.   

::: tip Academic essay?
The academic essay is a genre that can be defined in various ways. Make sure to consult your course material for formal requirements

::: 


### Popular science article  

The popular science article, like the scientific article, conveys insights and findings from new research. What is different here is that the popular science article’s goal is to communicate this information outside of the scientific research community to a more general audience. Popular science articles often use journalistic language. The level of verifiability, documentation and description of methods is slightly lighter than you would find in a scientific article. Similar to the essay, the use of metaphor is common in the popular science article.   


### Textbook 

A textbook is an introduction for students into a subject and its surrounding discourse. A textbook often includes current theories as well as a short history of the subject. The aim of a textbook is to include students in the academic community. Importantly, a textbook does not necessarily decide what is "the correct knowledge" in a subject area, but rather represents the theories that the author considers to be the most important according to their experience.  


### Thesis 

A thesis is a longer work on a topic or idea within a subject area, normally a master's thesis or a doctoral dissertation. Specific requirements apply to methods and use of theories, and for a PhD thesis the research community expects new results or perspectives.  

Bachelor theses and other smaller student papers should only be used for inspiration. You may for example use them to find relevant literature, and as examples for organising your own assignment. 


## Online resources  

There are many different websites that can be used to find relevant information on a given topic. News sites offer  the latest news, and it can be helpful to see how your topic is covered in the media. You can also find great quotes on different sites relevant to your field. However, there is a lot of information online, and many sites are less than ideal as sources in an academic context. 


### Encyclopedia vs Wikipedia 

Encyclopedias contain trustworthy, reputable information and background material on a variety of topics. The articles are compiled by subject editors and experts across a range of subjects and utilize reference lists. Encyclopedias are available in printed and electronic editions. 

Wikipedia is an online encyclopedia that is created and edited by anyone who chooses to. Feel free to use Wikipedia as a starting point to get an overview of a topic and to look up the sources used there. The basic rule is that you should never use Wikipedia as your sole source, but you can use it to compare perspectives against other sources. Make sure to check what you find in Wikipedia against reputable academic sources. 

Encyclopedias are good sources to use when defining words and concepts or to look up a topic. 


### Blogs 

A blog can be an easy way to get an overview of discussions in different forums. You can subscribe to blogs with, for instance, an RSS feed. Be aware that the style of a blog can often be in diary form and discuss the writer´s personal experiences or opinions. These are not always based on verifiable and scientifically documented claims.   

::: tip Handy tip! 

Check to see if there are any professional blogs in the area you're researching. For example, see the blog Forskning.no. 
:::


### Op-eds 

An op-ed is a relatively short text of an enlightening or reasoning character, usually in a newspaper. Op-eds are often used by researchers to bring research-based knowledge into the public. Op-eds, just like encyclopedias and Wikipedia, can be a good place to start to get an overview of a given topic and can help to give you a better understanding of the topic. 

### News

News broadcasters and other sites that report the latest news may be useful for getting a perspective on how topics are addressed. Note that, in the same way as op-eds, popular science and essays, news reports are not academic and should be avoided as part of argumentation and discussion. 


### Radio and TV programs 

Radio or TV programs can also be a good place to get fresh angles for your assignment. If you are going to write about psychiatry, but are unsure how to approach it, a documentary or semi-documentary can be a good source of inspiration. 

::: warning NB

If you want to use radio or TV programs as a source, shorter clips can be used without special permission, but for longer clips you must contact the filmmaker or producer for permission. 
:::

### Pictures from the Internet, photography, illustrations, artwork  

Pictures, illustrations and artwork may all be useful for your work. You may want to illustrate a point, or the picture itself may be a source of information. As a general rule, you cannot reuse pictures without permission. However, there are exceptions, and illustrations you can use freely are e.g. royalty-free images, Creative Commons images, or images in the public domain. Always read the terms of use! 


### Fieldwork and other observational studies 

In different fields and subjects where participant observation is useful as a method, informants are often an important source.  In the following video, Odd Are Berkaak, a professor in social anthropology at UiO, describes (in Norwegian) how to present your informants in your text.


<Video id="ktM1r1Ca5Tc" />


## Exercise: Which websites would you want to use? 

::: oppgave Exercise 1 

You are just starting on a task, and have decided on an overall theme.  You are looking for inspiration and ideas for what different angles you take on the theme.  Where do you start? 
:::

::: oppgave Exercise 2 

You have begun your discussion section and are looking for a source that helps to build up some of your arguments.  What kind of source makes your reasoning stronger?   
:::
 
