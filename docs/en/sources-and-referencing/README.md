---
title: "Sources and referencing"
date: "2012-03-16"
prev: "../writing/formal-requirements"
---

# Sources and referencing  

All research uses the work of others as its starting point – what we call ‘sources’. Good academic work is characterized by use of relevant, scientific sources and builds on existing knowledge. Without references to existing research, disciplinary knowledge and relevant information your assignment will be detached from the scholarly community. Any sources you use will help to lay the foundations for the assessment of your text.


<Figure
  src="/images/illustrasjoner_kildehenvisning_500x450.png"
  alt="Dialog boxes hanging over an open book"
  caption=""
  type="right"
/>

The best sources to use will vary from subject to subject, and from assignment to assignment. Relevant sources can be found everywhere: in books, articles, websites, news articles and maps. Use your problem statement as a guide, and ask your tutors, fellow students and researchers within your field to help you find sources that are appropriate for your discipline. 

# Why refer to the work of others?

All forms for argumentation which are not based on own material or own reasoning, must always be refered to during writing, and at the referencelist. This may be others opinion, material of numbers, models, results or conclutions. 

To help your reader easily locate your sources, use in-text-citations throughout your text, and collect all the references you have cited in a list at the end. The reason for this is both to make it possible for a reader to check the sources you are using, and to read more about your topic if they wish.

Careful documentation of sources enables the reader to quickly:

- look up the sources themselves
- check facts and the accuracy of your results
- find further information about the topic

Another important reason for having proper references is to avoid plagiarism. Plagiarism is the use of others’ results, thoughts and ideas as if they were your own. This is considered intellectual theft according to intellectual property laws.

Extensive and/or deliberate plagiarism is considered cheating. According to [Universitets- og høyskoleloven (Norwegian University and College Law)](https://lovdata.no/dokument/NLE/lov/2005-04-01-15) §§ 4-7 and 4-8, this can lead to failing the assignment and expulsion from your module, with potentially severe effects for further studies. All educational institutions are aware of problems related to cheating, and use specific software to detect plagiarism. 
