---
title: "Study skills"
date: "2012-03-16"
prev: "../searching/systematic-searching"
---

# Study skills

Reading and writing are closely related activities in all types of academic work. In order to write a good thesis, you need to be a good reader, and you need to be conscious of _how_ as well as of _what_ you read.

<Figure
  src="/images/illustrasjoner_lesing_500x450.png"
  alt="Books flying into the air"
  caption=""
  type="right"
/>

The most important thing you can do to improve your reading is to actually _read_. People who read a lot on a regular basis will usually develop good reading and writing skills. Our number one advice to get started with your studies is simply this: Get reading! There is no other way to become a good student.

Learning a subject involves getting familiar with texts that communicate scholarly insights, methods and areas of debate. Of course, reading is not the only way to learn about a subject: You will also attend lectures, discuss and debate at seminars and study groups, and write papers.

Academic work is a combination of effort, imitation and personal strategies. In this section you will find basic advice about how to approach your studies and various suggestions to bear in mind. You can adopt a pick-and-choose approach to the following advice – the same strategies will not suit everyone.

The verb "to read" is linked etymologically to the verb "to pick". The Latin word _lego_ means "I pick", as well as "I collect" and "I read".

Happy reading!
