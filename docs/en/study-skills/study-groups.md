---
title: "Study groups"
date: "2013-02-21"
---

# Study groups

Discussing texts with others is a good way to engage actively with a text. You are likely to read more attentively if you are planning to discuss the text. Discussions also let you practise applying concepts and articulating arguments.

<Figure
  src="/images/kategori_studier5-1.jpg"
  alt="Study group discussing an academic text"
  caption="Study group. Photo: NHH"
  type="right"
/>

Plan in advance; agree on a topic and text (or texts) to discuss, and assign different texts or portions of text to different members of the group. Each person is responsible for presenting their material to the rest of the group. By taking turns, each group member will get practice in both explaining and commenting orally on academic material. It is also possible to have spontaneous discussion sessions, but the discussions in an unprepared study group are likely to degenerate into undirected “chat”.

The earlier you form a study group the better. Start with reading and discussing different assigned materials. Once you start writing, [read each other’s texts](/en/writing/the-writing-process/writing-groups/) instead. Comments from fellow students can be very useful.
