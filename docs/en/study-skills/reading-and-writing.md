---
title: "Reading and writing"
date: "2013-02-21"
---

# Reading and writing

The purpose of written assignments in higher education is to develop your own independent written work based on what you have read and learned. One way to create your own text is by restructuring elements from texts written by others and disussing arguments presented in other texts. Reading while writing implies reading from the perspective of a research question and actively using what you read to develop your own analyses and argumentation. Through the reading process, you collect material that you can use in your own writing.

Keep in mind that the material you use is taken out of its original context. When you refer to or cite another text, you make it your own. You are responsible for everything in your own text. However, you cannot simply appropriate other people’s thoughts and writings as if they were your own. You need to distinguish clearly between your own ideas, paraphrases (indirect quotations) and direct quotations. In other words, you need to master basic [citation techniques](/en/sources-and-referencing/how-to-cite.html).

## Read your own text and those of others

- When writing a thesis, it is important to read it through several times. Be critical, and ask your own text the same questions you would ask other texts: What is the point I want to make here? Is the research question clear? Does the text provide an answer?
- As with other texts, it is often a good idea to put the text aside for a while occationally, and then return to it to look it over with fresh eyes. Is anything unclear? Is it entirely clear to the reader what I am investigating, what I want to demonstrate and/or argue, which ideas are my own and which are those of other scholars, etc?
- Read your text out loud. This will help you discover problems your readers might encounter. Are the sentences too long? Is the text repetitive? One can easily get lost in details when looking at one's own text; hearing how it sounds may give you ideas for improvements.
- Let other people read and comment on your text. Why not ask both fellow students and someone who is not familiar with your discipline? They might discover ambiguities that you are not aware of. When you and your fellow students read each other's texts, you learn and find inspiration, and you may also become aware of weaknesses and mistakes in other texts that you want to avoid in your own. Thus, swapping texts can be useful for both reader and writer.

## Keeping track of what you have read

When reading a text, you may sometimes think, “That was interesting. I didn’t know about that – I'll have to remember it.” What are the best ways to make use of such glimpses of insight? This section contains advice on note-taking and other ways of combining reading and writing during your studies.

## Taking notes

- Make notes of what you read in a notebook or in a document on your computer. This will result in better and more informative notes than scribbling in the margins. It can also be useful to write up your notes into short summaries of what you have read.
- Adding keywords, highlighting and underlining the text you are reading is another effective technique for becoming a more active reader. A text that is heavily underlined or highlighted might give a false impression of thorough analysis, however. Underlining fragments of a text is easy. Extracting key pieces of information and organising it into a logical order requires more work, but the benefits of doing so are far greater.
- You can highlight sections of text and make notes in the margins. **Note**: Do NOT write notes or highlight text in library books!
- Try to get hold of the main message of a text, its argumentation and its context. You should make as few marks and notes as possible and keep in mind the purpose of your reading: Are you planning to appropriate a text’s contents and arguments wholesale, will you use parts of the text in connection with something else you are writing, or are you reading for an exam?
- Do not start underlining and taking notes until you have read through the text, or at least until you have read enough to understand the general direction and subject matter. (Cf. [Ways of reading](/en/study-skills/ways-of-reading.html "Ways of reading")). If you get involved in the details too soon, it can be difficult to understand the text as a whole. Mark and underline passages where the authors gather their arguments or where the main points are most clearly expressed.
- Adopt a consistent policy on the symbols you use for highlighting, to mark, for example, importance. Examples of marks and symbols include underlining, double underlining, circles around words or phrases, exclamation marks and crosses. Systematic highlighting will save you time finding key points in the text later.

**Note**: You may change your mind about the most important aspects of a text. The first time you work with a difficult text about a new topic, you may in fact simply be guessing what is important in the text. Be open-minded when you revisit the text, you might have overlooked something important the first time – perhaps even the most important point in the text.

## Repetition

Rereading a text will help you remember it better and understand the contents more thoroughly. If you are rereading to understand a text better, reread it as soon as possible. In this case it is important to understand the text as a whole, which can only be acheived by going back and forth through the text, working out how the various parts relate to each other. When first reading a text, we tend only to see its parts. Rereading helps us understand how these parts fit together.

While writing it may be necessary to return to a text because your understanding or opinion of it has changed. Perhaps you understand the argumentation better after reading something else? Perhaps new questions have come up in your own discussion? Perhaps the text can be useful to you in a different way than you first anticipated, because you have rephrased your research question.

## Writing a summary

The idea of writing summaries while reading is to provide a tool for repetition. Highlight the main features of the text’s argumentation and structure. Your summary should be [accurate and true](/en/sources-and-referencing/how-to-cite.html "How to cite") to the text in question. Present the issues and arguments contained in a text on the text’s own premises. A summary should be neutral – this is not the place to apply your own research questions to the contents of the text or to criticise it - but you should not refrain from rephrasing and saying things in your own words. When writing summaries, aim to reproduce the authors' claims and arguments in such a way that they might say: “Yes, that was what I meant.”
