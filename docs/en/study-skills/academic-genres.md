---
title: "Academic genres"
date: "2013-02-21"
next: "../writing/"
---

# Academic genres

When studying you have to read different types of texts: textbooks, reference works, scholarly and popular articles and essays, conference papers, official reports and theses.

## Be aware of different genres

In other words, the texts you read belong to different genres. Being conscious of genres will aid your understanding and interpretation of a text. Genre is also a useful concept when writing - look for inspiration in the texts you have read. If you are writing a term paper, bachelor’s thesis or home examination, the text should be more like an article than a textbook chapter. That is, the text should consist of arguments and discussion, rather than simply present factual knowledge.

In school one learns about literary genres (poems, short stories, novels, plays etc.) and non-fiction genres. There are both scholarly and non-scholarly genres within non-fiction. Non-scholarly examples are journalistic texts, such as editorials and news reports. The following section describes some genres of academic writing and outlines their particular characteristics.

## Some academic genres

_Textbook._ A textbook is mean to communicate established knowledge to students of a given subject. It provides a general introduction to the subject as well as relevant problems, concepts and theories. A textbook presents knowledge and explains how it has been used, and can be used. It informs and instructs. Even though a textbook can include discussion and argumentation, its primary objective is not to argue for or against particular positions, but to present the existing views within the subject.

_Scholarly article._ The purpose of a scholarly article is to present new knowledge or to provide new perspectives on an academic or scientific problem or object. The target audience is other scholars. A scholarly article is primarily argumentative. It claims that something is true or probably true and presents arguments to support the claim. Hence, a scholarly article has to present thorough and consistent reasoning. The reasoning must be properly substantiated, through references to empirical data or other research. Authors of scholarly articles should use acknowledged research methods, and also explain them.

_Thesis_. A thesis is a major piece of scholarly work. In some fields (especially medicine and the natural sciences), theses are frequently assembled from existing (published) articles. The thesis will then include an introduction that explains how the different articles are related to the overall theme of the thesis. In other fields (in particular the humanities and social sciences), most theses are monographs, i.e., a continuous text divided into chapters. In either case, a thesis is similar to a scholarly research paper in that it presents new knowledge to other professionals in the field, puts forward arguments, and is thoroughly substantiated. If you are writing a master’s thesis, this is the type of text you should aim to produce.

_Popular (non-scholarly) work_. Popular texts, in the form of either books or articles, are intended to communicate established knowledge to the “general reader”. Since such texts generally do not attempt to establish new knowledge or challenge accepted truths, there will normally be less argumentation than in a scholarly text. The author instead tries to bring the subject to life by devices such as stories, anecdotes and illustrations. Sometimes the whole text is constructed as a narrative, for example in the form of a biography of a leading expert, or by recounting historical events.

_Encyclopaedia article_. The purpose of an encyclopaedia article is to present established knowledge neutrally, concisely and clearly. The target audience is usually general readers, as well as specialists, although there are also scholarly encyclopaedias that require the reader to have specialist knowledge. Encyclopaedia articles are not generally intended to persuade the reader by way of reasoned argument, and they do not clarify or instruct as a textbook would. Encyclopaedia articles are intended primarily to be informative and descriptive. They provide information, define words and concepts and describe relevant circumstances.

## More about genres – and a bit about speech acts

After reading the brief descriptions of academic genres above, consider the following:

- The assignment of genre to a text is a rough classification by characteristics and context. The very act of classification, however, is also a genre, and its utility varies with the intended purpose. The classification outlined above serves to offer some basic guidelines.
- In practice, the above genres are not always completely strict or isolated. We often encounter texts that _combine_ genres or _breach_ the conventions of specific genres. A biography can have both popular and scholarly elements, and should perhaps be referred to as a “scholarly biography”. A given article might be more inquisitive, argumentative, personal and linguistically original, than a standard scholarly research paper, and is perhaps better described as an _essay_. In this area there are often differences both in terminology and between professional traditions.
- In the summary above, we distinguish between genres by _purpose_ and _target audience_. We also use a number of words (verbs) to describe the different genres, or what texts in these genres _do:_ communicate, explain, present, argue, inform, describe, narrate etc. A genre of writing can be said to be characterised by the dominance of one or more speech acts. The _primary purpose_ of an encyclopaedia article is to inform the reader. The _primary purpose_ of a scholarly article is to present arguments. This does not mean that the article cannot _also_ be informative. The provision of information, however, is subordinate to argumentation.

When reading an academic text, we can try to identify the different _speech acts_. Language researchers (linguists) sometimes talk about different “text types”. A common classification scheme (which derives from 19th century American teaching of rhetoric and writing) uses four “modes of discourse”: exposition (i.e., the presentation of an idea and relevant evidence), description, narration and argumentation, abbreviated to the acronym EDNA.

When reading a text, you should ask the following questions. What is the text doing now? Is it explaining? Is it describing? Is it narrating? Is it arguing? By asking these questions you will deepen your understanding of what is going on in the text. Such a reading will also actively prepare you for your own writing.
