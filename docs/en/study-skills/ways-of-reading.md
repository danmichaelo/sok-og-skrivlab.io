---
title: "Ways of reading"
date: "2013-02-21"
---
tags: 
  - "note taking"

# Ways of reading

There are many ways of reading, which are determined partly by what is read, but also by the purpose of reading the given text. We read novels differently from introductory works on statistics. However, whether we we read a novel for pleasure or for literary criticism will also affect how we read it.

Pleasure reader get immersed in the story, and get to know the characters and wonder about the outcome. A reader analysing a novel, however, looks for answers to specific questions. How is the narrative structured? How are the characters introduced and described? What literary devices does the author use? What type of novel is it?

## Reading objectives

- Suppose you are exploring a new field or topic. You begin by looking for general knowledge by reading _broadly_ rather than deeply. You study reading lists to get familiar with the specialist literature (books, articles, compendiums). What does the library have, and are there any relevant websites?
- Reading broadly is useful in the initial phase of the writing process, both for a fixed assignment or to develop your own research questions.
- If you intend to write a thesis on the topic, you will eventually need to narrow your focus. Look for material that is directly relevant to your topic and your specific research questions. Ask yourself: can I use this in my text? You will also need to read the texts more carefully than in the earlier exploratory stages of reading.
- Engage in dialogue with the texts you read. Studying involves not only gaining knowledge, but also learning to employ it in different manners and situations. All your reading is ultimately directed at writing something, be it a presentation for your study group, a semester assignment, a project report, an exam paper or a thesis.
- Not all texts are of high quality and not all texts are equally relevant (see [Source assessment](/en/sources-and-referencing/source-assessment.html)). Learn to evaluate your sources, critically and independently, regarding not only their content, or allegations, but also the text as such. For example: what makes a text so important that it becomes required reading for a course? What are the relations between the different texts on the reading list?

## Reading a text in several different ways

There is no one-size-fits-all solution for getting the most out of a text. Readers are not all alike. Practise being an active reader. Try to vary your ways of reading. One recommended strategy is to read the same text three times, in three different ways.

## Reading a text three times

- **_Familiarise yourself with the text_.** Browse through the text – form a provisional opinion of its genre and subject. If it is an article, look at the headings, subheadings, the summary and the reference list. If it is a book, study the table of contents, the foreword, the subject index, the names index, the back cover blurb and anything else that describes the text. Read some isolated paragraphs. For example, read the first and last paragraphs of an article or the opening paragraph of each chapter in a book. By doing this you will be better prepared when you actually read the book, as you already have an idea of its contents.
- **_Read quickly through the text_.** The next step is to quickly read through the whole text. Do not take notes yet (although you may add some small symbols in the margin – dots, lines, exclamation marks – to mark particular points of interest). If you do not understand everything, keep reading. If you do not understand anything at all, keep reading. You will get more than one chance to return to the text later. If you are reading a book, apply this strategy to, for example, one chapter at a time. It often helps to deal with the text in smaller chunks when reading books or other long texts (such as theses or official reports).
- **_Read the text closely_.** Immerse yourself in the text. You are now reading to understand the text to the best of your abilities. Get a pen and paper – or open your laptop. Take notes while reading. Print your computer notes once finished. Keep all your notes together in a file. Over the course of the semester you build an archive of notes on the reading list. This archive is worth its weight in gold when preparing for exams, writing your thesis, or recalling what you studied in a previous semester. In addition, your archive is already providing you with a motivation to keep reading – you need to expand your archive!

Once completely familiar with the text, take a break. Make sure, however, that you [return to the text](/en/study-skills/ways-of-reading.html#reading-a-text-three-times) fairly soon – after a day or so, or perhaps a week – and give it another quick read. You will probably understand it differently. Also, it is always useful to discuss the texts you read with other students.

## Ask questions

Ask the text questions. Try to “force” it to provide you with answers. Trying to clarify something you wonder about or do not understand while reading makes the reading both easier and more interesting. Ask different questions depending on why you are reading; from general questions about the text’s agenda, message and argumentation, to the specifics of particular words and concepts, or what an example in the text intends to illustrate. The idea is not that the text will think for you, but that you use it as an aid in thinking about your topic and your thesis.

## Exercise

Look at a text from your reading list, and answer the questions: What is the topic? What is the research question? What do the authors discuss, what are their conclusions, and how are they reached? Can any of the conclusions be of use to you, when writing your thesis?

## Looking for context

Already when you were initially getting familiar with the text you came in contact with its context, or its frame of reference. The list of references include titles of other works and the names of other researchers. If the text is a book, its index might also include such information. On the back cover there might be comments by other researchers, extracts from reviews and so on. You also find many indications of context in the actual text. These include information about, debates with, and criticism of other researchers, as well as concepts and terminology common to the wider research community. Another clue lies with the authors. Who are they?

In short, the clues about context are there for you to follow. Identifying the frame of reference of a text helps you understand it better. You are directed to a wider academic context, in other words, your field of study. The difference between looking for the context of a text and getting an overview is that you are not simply familiarising yourself with the text and its immediate surroundings, but also with the wider territory it inhabits. Look for clues such as

- **The author(s)** of the text. Who are they? Are they published elsewhere? A search for their names will probably give websites, encyclopaedia articles or other material about them. Do they belong to a particular **theoretical school**? If so, is it possible to find out more about it?
- The authors refer to **other researchers**. What can you find out about them?
- What are the **key concepts** in the text?
- Has the text been discussed or referred to elsewhere, in other **books and articles**? Has it been **reviewed**?

For example, you read the following paragraph:

> “‘The Mediterranean may accordingly be said to be an inversion ritual as according to Gluckmann and a heterotope as according to Foucault…” (_Døving, R_., _2011, p.25)._

The word “accordingly” suggests that information appears elsewhere in this article both about “an inversion ritual as according to Gluckmann” and also “a heterotope as according to Foucault” – and perhaps this additional information will be sufficient to clarify the meaning of this paragraph. Even so, who are Gluckmann and Foucault? We cannot be sure that much will be said about them. Perhaps they are famous researchers and anyone practising in this field should know about them? And what do “inversion ritual” and “heterotope” mean? Although the author may provide an adequate explanation, consulting other sources is probably worthwhile.

## Looking for argumentation

Many of the texts you read are _argumentative texts_: articles, theses, and scholarly monographs (books). Most of the texts (written assignments) that you write during your studies will also be argumentative. Accordingly it is important to understand what we mean when we describe a text as [argumentative](/en/study-skills/argumentation-in-text.html "Argumentation in text").

## Notes, summaries, re-reading

Taking notes, writing summaries and re-reading are all different ways of working with a text. We have already recommended taking notes from the texts you read during your studies in order to build up an archive of notes for later use. The archive will be even more useful if you write up your notes (or parts of them) into brief summaries. Read [more on taking notes](/en/study-skills/reading-and-writing.html), writing summaries and re-reading texts.
