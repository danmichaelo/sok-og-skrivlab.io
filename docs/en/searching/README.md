---
title: "Searching"
date: "2012-03-14"
---

# Searching

This section explains how searching for information can help you get started with your thesis and how well-planned searches can help you define the theme and scope of your project.

<Figure
  src="/images/illustrasjoner_sok_500x450.png"
  alt="Magnifying glass over different information resources"
  caption=""
  type="right"
/>

Academic information retrieval is a process that requires careful planning. We relate the information we find to our prior knowledge. This expands our knowledge, giving us new ideas and a better understanding. Our information needs develop from a vague idea, to a specific need for information that deepens our understanding and furthers our research.

Before you start searching consider the following:

- What sort of information will I need to answer my research question?
- Where can I find this information?
- What is the best way for me to get an overview of the existing research in the field?

Your information needs vary as your project progresses. You will need different types of source material at different times and the criteria you use for evaluating the information you find may change.
