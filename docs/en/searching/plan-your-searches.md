---
title: "Plan your searches"
date: "2012-03-16"
---

# Plan your searches

## Getting an overview

During the early stages of your thesis work, you need to gain an overview of your chosen field. This will help to clarify your research questions, your methods and your general approach. At this point you may find it useful to skim through a few different sources. Some of them will continue to be valuable resources as your project progresses, while others will only be useful at the beginning.

## Finding background information

- General encyclopedias such as Wikipedia and Store Norske Leksikon cover a wide range of fields and can direct you to more comprehensive sources.
- Field-specific encyclopedias (for example _International Encyclopedia of the Social & Behavioral Sciences_) provide thorough introductions. The authors are experts in their own fields and have charted and reviewed the central literature.
- Text books from the syllabus and reading lists can introduce and guide you to sources that can take you deeper into the subject.
- Through the news archive, [ATEKST](https://web.retriever-info.com/services/archive.html), and the National Library's [digital newspaper service](https://www.nb.no/aviser), you get access to Norwegian public debates. Both archives are available through most libraries in Norway.
- Public information such as reports, government white papers and statistics are easily accessible on the internet, for example see [www.regjeringen.no](https://www.regjeringen.no/), [Statistics Norway](https://www.ssb.no), [the World Bank](https://www.worldbank.org/), or the [OECD](https://www.oecd.org/).

## Locating scholarly literature

Once you have read up on the subject and the research question begins to take shape, there will be a need for information that goes deeper into the subject. The academic community expects you to base your assignment on scientific sources, and here, articles in peer-reviewed journals are the most important sources. The term _peer review_ means that the manuscript has been reviewed by expert scientists prior to publication.

## Selecting databases

Through your library's websites you have access to databases covering a wide range of disciplines. A database is an electronic archive that contains different types of sources. Some databases are interdisciplinary, while others only cover a specific field. The field-specific databases provide better coverage of the literature in that field compared to the more general databases. Familiarize yourself with the databases that are relevant to your subject, keeping in mind that no databases cover everything. They overlap and complement each other. Therefore, it is important to use multiple databases to get an overview.

Below you will some interdisciplinary databases that can be used as a starting point for searching before moving on to the field-specific databases:

- [Oria](https://oria.no) is the research library's search tool. Here you will find, among other things, textbooks, master theses, dissertations and journal articles.
- [Google Scholar](https://scholar.google.no) is the academic version of Google. It indexes scientific literature from reputable publishers and research-based databases.
- The article database [Norart](https://www.nb.no/baser/norart/) provides an overview of Norwegian and a selection of Nordic journal articles. The archive covers both popular science periodicals and scientific journals, so you need to make a critical assessment yourself.
- [Idunn](https://www.idunn.no/) covers journal articles from journals published at the university publishing house. It is available in most libraries in Norway.
- The publishing archive [NORA](https://nora.openaccess.no/) and databases such as [Cristin](https://www.cristin.no/) (Current Research Information System in Norway) gives you an overview of research activities in the health and institute sector and the university and college sector.

## Devising a search strategy

During the process of searching, you will encounter an exciting world of knowledge. However, to avoid random and unsystematic searches, you may want to devise a plan for your searching. This can save time and ensure that all the important elements of the research question are included. A good search strategy should describe which search terms you have used and how these are combined. See more under [searching techniques.](/en/searching/searching-techniques.html)
