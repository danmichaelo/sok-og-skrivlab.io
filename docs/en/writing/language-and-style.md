---
title: "Language and style"
date: "2013-02-21"
---

# Language and style

Academic language should be clear, unambiguous and objective. “Objective” does not mean that you avoid taking a position; rather, it means to expose its foundations (reasons, evidence). Inexperienced writers are often tempted to embellish their language, using complicated expressions and technical terminology. As a rule of thumb, however, you should choose ordinary language as long as it is adequate. Scholars who have achieved classical status often write in a plain and direct style. This is precisely why – regardless of changing literary conventions – they have been widely read over the years.

Many academic studies are, by necessity, demanding to read. This is partly due to their high level of specialisation and partly due to formal requirements such as detailed descriptions of methodology and findings, numerous references etc. This means that the authors have to put down a good deal of work in order to produce a readable text.

## Level of style

Who is your audience? What can you assume that your reader already knows, and how many definitions are needed? For example, are you writing for your supervisor or for a general reader? The general advice is to aim somewhere in between, and to write as you would do for a fellow student.

### Active – Passive

Many students and researchers use the passive voice of verbs in their texts. Sometimes this is necessary, but too much passive voice makes for a heavy-going text. Moreover, passive constructions often give rise to other problems. For example, you are likely to end up with long sequences of words strung together by prepositions. For example, “_… investigation of questions concerning a reduction in the occurrence of …”_

**Example of a passive construction:** New results in this area are continuously produced by the research group.

**The same sentence using the active voice:** The research group continuously produces new results in this area.

Use of the passive voice tends to conceal who is doing the action. In a methods section, this is often the norm since the results should be reproducible by anyone. However, there is a common misunderstanding that sentences using the passive voice are more “objective”, because the author avoids saying “I” or “we”. It is sensible to vary your writing style as appropriate. Overuse of the passive voice makes your text heavy to read, and gives a woolly, bureaucratic and "mystifying" sound . Do not feel that you have to avoid it altogether, however, as overuse of the active voice also becomes tiring for your reader. We do not always need to be reminded of the person of the researcher through the use of “I” and “we”.

::: oppgave Exercise

**Rewrite the following in a more direct style:**

- X is characterised by importance
- Y is characterised by reliability
- X entails correctness
- Research is carried out
- An investigation is being undertaken
:::

### Can I write "I"?

In some fields, writing in the first person (i.e., using the words “I”, “me” and “my”) is strongly discouraged. If this is the case in your field, you may have to write your text using words such as “one” or “we” or using the passive voice. It is important to be clear, however, that using the word “I” is not the same as being personal or subjective.

We should distinguish between the private or personal “I” and the “I” as the author of the text. For example, when you write “I will now explain …”, this is _not_ a personal statement. In most disciplines the use of the authorial “I” is fine in, e.g. [pointers for the reader](/en/writing/language-and-style.html). Writing in the first person will make the text less “stiff” than using the word "one" when referring to yourself.

The personal “I”, however, has no place in an academic text. This does not mean that you cannot put your own personal stamp on the text. It simply means that you should do this by using the means that the various academic genres put at your disposal. For example, by choosing to discuss interesting and important research questions, by presenting convincing reasoning, and by using good examples.

In academic texts you may also find a third type of first person, the researcher “I”. Once again, this is not a personal reference, but refers to the person who has, for example, collated data or carried out experiments.

::: oppgave Exercise

Read several pages of a student dissertation and highlight the word “I”. Identify occurrences of the personal “I”, the authorial “I” and the researcher “I”. For comparison, read a research article that uses the word “I” and carry out the same analytical process. How did the results vary between the two texts?

**Note**: Remember that there may be good reasons for the use of “I” in a student dissertation. These reasons may not be relevant in the case of a research article.
:::

Recommended reading: [Passive-voice](https://writingcenter.unc.edu/handouts/passive-voice/)
