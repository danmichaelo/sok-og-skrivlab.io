---
title: "Structure"
date: "2020-06-23"
---

# Structure 

The structure of your assignment is the ‘skeleton’ that holds different parts of your text together. In order to ensure that your assignment is coherent and flows well, it is important to understand what function your different chapters or sections have in relation to each other. Everything you write in your assignment must fulfil a purpose. Here you can find advice on what the different chapters/sections in an assignment should contain, focusing on their different functions. 

There are many ways in which you can structure an assignment. Earlier on in your studies, you will often encounter “explain and discuss”-style assignments. In the health and natural sciences, the [IMRAD model](/en/writing/the-imrad-format.html#introduction) (Introduction, Methods and Materials, Results and Discussion) is often used. In the humanities and social sciences, you usually have more freedom to decide how you want to structure your text. 

::: eksempel A good assignment is characterised by

* All questions that arise are answered or explored 
* Things are made clear for the reader 
* Everything that is introduced and explained has a function 
* Theory is used in order to analyse and interpret data (if relevant) 
* The methods chapter is clear and concrete 
* The findings are fully justified and explored 
* The discussion brings together the empirical data, theory and methods 
* The conclusion logically follows on from what has been written before 
:::

 

## One thing at a time 

In order to write in an effective and structured way, it is important to distinguish between different types of writing such as _presentation, interpretation, analysis_, or _discussion_. Address one thing at a time and avoid mixing explanation and discussion in the same statement. This will help you produce a clearer text. 

In a theoretical assignment, [argumentation](/en/writing/argue-explain-discuss.html) and discussion are imperative. You can either discuss ideas and concepts along the way or separate out the discussion in its own section. The latter is most common in scientific journals. Make sure you differentiate between explanation and discussion. 

It is also important to mark what is the reproduction of others' ideas and what is your own interpretation and reasoning. This is to help the reader follow your reasoning and show that you can make critical assessments where necessary.  This is central to remaining objective and critical.  

::: oppgave Reverse Outline 

If you are wondering if your text has a clear structure, you can mark the paragraphs with post-it notes. Keep track by writing down key words/ideas of each paragraph. If a paragraph has more than one point, consider splitting it or moving the content elsewhere in the paper. What belongs together (logically) should also be placed together. 

Then you can look at the context. Are the points in the right order? Is there something that needs to be moved? Use the Post-it notes and try different approaches. 

:::

In a research paper, discussion usually comes after an explanation, and ties together the thesis (Introduction, method, results and discussion). See more about discussion under [the IMRaD model](/en/writing/the-imrad-format.html). 

Based on the discussion, you can draw conclusions. Conclusions must be valid, they must logically represent what they are based on, in empirical research, the conclusion must also have scientific validity.  

## Paragraph structure: The TES model 

Below you will find the TES model (Topic-Elaboration-Stress), which is a way to structure an argument in a paragraph.  This format can also be used for non-argumentative paragraphs.  It is important to remember that there is not one single correct way to write or structure a paragraph, but the model can be a helpful tool in guiding _your_ own writing.   

**Topic – What is this about?**

The first thing that needs to be clarified when you are making an argument is what will you be talking about, also known as the topic.  The first sentence of a paragraph should make it clear what it is you are writing about, and this is why we call it the “topic sentence”. The topic sentence is usually a statement or build up to a statement that you will later expand upon and describe further.   

**Elaboration – How and why** 

In academic writing it is important that whatever claims or statements we make are supported with evidence.  The evidence you use might come from empirical (data, facts, etc.) or theoretical justifications.  Statements which cannot be backed up, cannot be used in the argument, but instead fall under the category “speculation”. In the discussion, the strength of the argument is usually discussed, i.e. was it strong or weak?  Are there other possibilities or reasons?  It is helpful to provide different examples in this section to make it clearer for the reader. 

**Stress – What does this mean?**

The ‘stress’ or emphasis of an argument becomes most clear when you use examples. By using examples, you can show how you understand a topic in practice and provide the reader with nuances in the form of recognition and associations. Well-chosen examples can save you a lot of additional explanation. Another way in which you can demonstrate the stress of an argument is to draw clear conclusions in your text. 

 

## Topic-Elaboration-Stress 

In total, you get the following formula – Topic, Elaboration and Stress, T-E-S. The stress you place at the end of a paragraph may well serve as a stepping-stone to the next paragraph which then introduces a new theme. Your text can therefore be built up with several paragraphs which would follow the formula T1-E-S, T2-E-S, etc.  

Many of the paragraphs on this page are in fact examples of the T-E-S model.  

::: oppgave Exercise 

Read five paragraphs in any introductory book from your reading list and see how many of these follow the T-E-S model. 

:::

## How to create a good structure 

It takes a lot of work to create a text with a clear structure. In this film you can listen to Professor Ingvild Sælid Gilhus explain how she revises a text. 
