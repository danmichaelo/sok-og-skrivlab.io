---
title: "Getting started"
date: "2020-06-23"
---

# The writing process 

Working on a large body of work requires dedication to the creative process. These processes have different phases where flow and inspiration alternate with slower and more considered periods of writing. Doubt and uncertainty will often come up when the work hasn’t quite found its shape, whereas at other times, you will find inspiration easily. Enjoy it – and know that it is _normal_ for the process to ebb and flo.  

You may also experience 'text turmoil' – a discomfort that is difficult to put into words – that comes from the text itself. This could indicate that you need to rethink, and perhaps restructure what you’re writing. As soon as you begin to tackle the problem, you will find that it loosens up and ideas begin to fall into place. The structure will fall into place when you can’t move around the different parts anymore. 

::: tip How do I start writing? 

Inexperienced writers often believe that they cannot start writing until they have a clear idea of what everything will look like. They may be stuck with the idea that ‘good writing’ comes from putting  perfect sentences together one after the other. This kind of thinking will keep you from getting past the preparatory stage. In reality, everyone has to rewrite their text several times. In order to write well you have to write, so just get started, and trust the process.  
:::

Researchers are always asking the question ‘why’? This is your starting point as a student, too. The researcher writes in order to explain something that hasn’t been explained before, to fill a hole in our understanding of the world. They question phenomena, pick apart ideas with their peers, explore possibilities, ask questions, take notes and write down their ideas. So do as the researcher does, let your curitosity drive you to ask why. As your work becomes clearer, your uncertainty will begin to resolve.  

 

## Getting Started 

Getting started with writing is more important than having complete control over the end product. There are various writing techniques you can use to develop your initial ideas on a subject, for example brainstorming, mindmapping, drafts and free writing as described below. When you start writing, you will probably find that the project becomes much clearer to you. 

Feel free to start writing about what you think is fun or most interesting. Write a little at a time. If you take breaks BEFORE you get stuck, it is easier to pick up the thread again. Feel free to present your text to others, such as in writing groups, and ask for feedback even if you are not completely satisfied with your writing. Good writers spend time revising and often have to restructure their texts several times.  

There is no right or wrong way to begin the writing process. The best thing to do is the thing that works for you. Do you need to mull ideas over for a long time before producing a relatively polished piece? Or do you just need to write even if you don’t know where its going, and then spend your time revising and tweaking? Whatever works for you is your right way to write.  

There are (at least) two strategies for producing text: 

 

::: eksempel Write before you structure 

Write down everything you know and wonder about regarding the topic 

Go through what you have written and identify key words and themes. Divide your work into paragraphs and order them in a structured manner 

Use this key as a shortcut to structuring your text. 

:::

::: eksempel Structure before you write 

Create an outline with headings 

Fill in your outline with detailed text 

Revise, revise, revise 

:::

## Techniques for getting started 

Below are some different techniques that can be used to approach a problem and outline your paper. 

 

### Brainstorming 

Brainstorming is a great tool for getting an overview of what you already know about a topic and clarifying where you need to research more. It can also be used to identify themes and make outlines. First pick a theme, topic, or idea and write down anything and everything about it, without cencorsing yourself. As you go through the exercise, different key words and phrases will pop up. Remember these, after the brainstorming session: 

- Write down key words and phrases 

- Draw relevant figures/graphs/charts 

- Note important book titles, journal articles and different types of research 

Ideas can pop up anytime – always have pen and paper ready! 

::: tip Tips: 

You might not always have a pen and paper to hand, but you probably have a cellphone close by. Write down your ideas in a text message, a note page on your phone, or you can even make a voice recording. 

:::

### Mind map 

- Write the main idea/topic you will be working with in the middle of a sheet of paper. 

- Draw lines or branches that connect to the main idea, and write key words on each line.   

- Make even smaller lines and branches that have more detail and are more focused.   

- Write down any new ideas or connections that you come up with – be spontaneous!   

- Finally, look for any connections or ideas that you want to highlight, focus on, and potentially develop further.   

 

::: tip Tips: 

Use colors to highlight different relationships and connections, and to help emphasize your thoughts. 

Using pictures or symbols can be helpful. 

Use lowercase letters.  Lowercase letters are easier to read and remember than uppercase letters.   

:::

### Developing ideas 

When developing your ideas, it can be helpful to have a creative, private text to help you in this part of the writing process. Idea development is about learning through the writing process, where you are able to develop new ideas and engage with them. When you aren’t focused on the finished product, you are more  able to explore new possibilities and discover new ideas that you may want to focus on.   

- To start, write down everything you know, and also what you want to know more about that relates to your theme. This can be done in a more or less coherent text.   

- Choose some ideas and contexts that you want to focus on 

- Write without worrying about how the text is developing and its structure.  The goal of idea development is to produce and develop ideas away from critical eyes and thoughts.   

### Free writing 

Free writing is a technique to help move your ideas forward.   

- Chose the topic you want develop, and make this the heading. 

- Write non-stop for 10–30 minutes without lifting your fingers from the keyboard or pen from the paper.   

- Afterwards, read through the text, highlight any key points and begin to structure your ideas.   

- Divide the text into smaller sections, and create new headings for these   

## From theme to thesis 

Sometimes we are lucky enough to know exactly what we need to investigate before starting an assignment, whereas at other times we need to think, make notes, conduct a literature search, and read for a while before formulating a problem statement or a research question. Often a research question is tweaked or changed several times during the writing process. There are several ways to get started with writing, but it can be helpful to have a problem statement or research question in mind as early as possible in the writing process. 

Brainstorming some initial ideas can help you to formulate a problem or research question. Write down all the questions and ideas that come to mind – you can re-examine and take some of these ideas out at a later stage. As you get to know a topic better, you will gradually be able to ask more precise questions. Perhaps new questions or angles will appear that you had not previously considered. Think carefully about: what interests you? What engages you most about a particular topic or idea? Try to formulate full answers to these questions. You might want to use these ideas later as a starting point for your introduction. 

::: oppgave Exercise 

Write down at least five different versions of the problem statement you are working on 

Choose the two best ones 

In a new document, write down five new versions based on these two 

Again, choose the two best 

Start another new file and write down five further versions of your problem statement 

Select the two best 


Continue this exercise until you have a problem statement that works for you, and that can be successfully answered. 

::: 


### How to keep going 

To help you keep track of your work, it can be a good idea to maintain a log or a diary describing what you have done, and what remains to be done. You could also start a blog to get input (from others?) throughout the writing process. 

Some blogs have a very personal style, similar to a diary, while others are intended as a  professional forum for discussing ongoing work. Find what works for you. 

::: oppgave Talk about your writing  

Talk about your assignments with your fellow classmates and anyone else you would like. Putting your ideas into words and explaining them to others can be a very helpful process. Why do you think this topic is interesting? What do you want to find out? Are there any problems you expect to encounter? How can these be dealt with? Presenting drafts for your classmates is an excellent way to get started. 

:::

Good luck! 

 

 
