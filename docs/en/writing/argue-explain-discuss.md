---
title: "Argue, explain and discuss"
date: "2020-06-23"
---

# Argue, explain and discuss 

Argumentation is what drives your text. What is it that you want to say? What is the main point of your argument, and how will you substantiate it? Argumentation means clarifying what it is that you are claiming in your text, and what you are building this claim on. In other words, it is the justification of your claims. The film “Saying what you mean” provides one example of how this can be done. 

<Video id="OWeAPxlxGnE" />

### Argumentation: Simply put 

A simple but usable definition of argumentation is **claim + justification**. This means explaining what your claim is, and then justifying it (with evidence, reasons, materials etc.). This definition can be helpful in thinking about how to structure arguments in your assignment. When making an argument the best way to back up your claim is by providing different examples. You will find more information on how to structure an argument at the bottom of this webpage.   

## Explain and discuss  

Many student assignments follow the formula ‘explain and discuss’. In these kinds of assignments, argumentation is typically focused on **comparing, using** and/or **evaluating** different approaches to a phenomenon (for example different theoretical perspectives). In the first section of the assignment, you should explain something you have read, while in the subsequent sections you should discuss what you have explained. This can, for example, be about explaining a theoretical concept which will then be applied and discussed in relation to a practical phenomenon. It could also be about discussing two different theories or concepts and comparing them with each other. 

But what does it mean to ‘explain’ something, and what is ‘discussion’? 

### Explain with your own words 

When you are asked to ‘explain’ something in an assignment, this typically means to ‘describe’ or ‘present’ something. When explaining, you are not being asked to take a position or give an opinion on the concept you are describing. There is no need to say whether you think it is good or bad, or to pose too many questions about it at this stage. 

In an explanation you should summarise and reproduce what you have read, rather than expressing opinions. When you explain a theory, you should present the ideas of the authors in the most objective way possible. The test of a good explanation is that it would be approved by the author of the original text. Also, a neutral description at the beginning of an assignment lays the groundwork for you to go into more detail and provide a considered criticism later in your text, rather than giving either a negative or positive assessment right at the start. 

The text will be clearer and more logical for a reader if you begin a new paragraph at the point where you begin to discuss concepts and ideas, following on from what you have explained and described so far. 

::: tip Use your own language 

Reproduce what you have read in your own words; you can then go back and check that what you have written matches up with the original source. By doing this, you will have a much better flow in your text than if you directly copy the language used in the original material. 

:::

It is important in any assignment to show that you have understood what you have read. You can do this by presenting the main points of a source or text in your own words. It is therefore more important that you write in a clear and understandable way, than that you use advanced words and concepts from the literature. 

Remember that when you are explaining or describing something, it is important that you write in a way that is acceptable to any reader, including those who do not necessarily share your opinions. Any explanation or description that you give should be balanced, comprehensive, and not influenced by your own views and opinions. Read more about [reading and summarising](/en/study-skills/reading-and-writing.html). 

### Discussing: look for differences 

In contrast to an explanation or description, a discussion should be based on the development of your own views through reasoning and argumentation. The substance of your discussion can be taken from your own earlier descriptions and explanations. An interesting discussion may for instance occur when you present different, opposing claims and interpretations of a source, and evaluate their strengths and weaknesses. Remember that any claim you make in your text should be reasonable and supported by evidence taken from your original source material. 

In order to create a meaningful discussion, it is important to be clear on what it is you are discussing. It can be sensible to start with a **point of tension** in the literature you have already described. An assignment is often designed so that you are required to discuss different approaches to the same phenomenon, for example you may be asked to discuss two different theoretical positions on the same concept. It is far more interesting to explore the differences between theories or perspectives than to only look for similarities, although you may wish to examine the ways in which theories are similar in order to highlight their differences. Examples taken from the literature you have read are often helpful for supporting your arguments. 

::: eksempel Three tips for a good discussion 

* Don’t put forward too many questions 

Remember that if you ask a question in an assignment, the reader will be waiting for you to answer it! Asking a lot of hypothetical questions about a concept is not the same as discussing it. Lots of questions one after another can be very tiring to read. 

* Work systematically 

Use one paragraph per idea or theme, and finish one point before you move on to the next. Avoid mixing explanation and discussion in the same sentence. Everything you write must be understandable and logical to a reader. 

* Draw relevant conclusions 

Summarise or conclude with the main idea that you have tried to convey in your text. Do not write a bland or ‘empty’ conclusion that could work for any assignment. It should be clearly tied to the arguments you have built up in your text. 

:::

In some subjects it is common to explain a concept first and discuss afterwards. In other subjects it is preferred that you discuss throughout your paper. Both structures can produce good texts – the most important thing is that you understand when and where you are explaining something as opposed to when you are discussing, and that you don’t ‘slip’ from one to the other. For example, you should not reproduce an idea from the literature and at the same time say that you disagree: “Author A claims that X, which I think is wrong.” 

::: oppgave Exercise 

Look for argumentation and discussion in your course literature. See how the writer builds a claim by using statements and reasoning, and by setting different ideas (_positions_) up against each other. 

:::

 
## Structuring an argument:  What is your reasoning? 

The argument you make is backed up by your reasoning, and your reasoning is comprised of: 

1. A claim or a position - something you are arguing _for_ 

2. An _argument_, i.e. how you are backing this claim up 

3. An assertion that _brings together_ your claim and your argument 

We create an argument that is easier for the reader to understand when we follow this structure of combining a claim with an argument backed up by evidence.  Stephen Toulmin breaks argumentation down into 6 different parts.   

 

#### 1) The main claim 

In the main claim you explain what your research question is, and what you expect the answer might be.  There are several different ways that you can do this in your introduction, whether that is with a research question, a hypothesis, or a thesis statement.  The main claim is whatever you have concluded at the end of the paper or the assignment.   

What is your main claim? 

#### 2) The argument (evidence) 

The argument can be based on different forms of empirical **evidence**, such as references from established authors or other sources (historical sources, interviews, statistics, photos, maps, etc.)  The evidence in your argument is how you **back up** your claim and position.   

What is the argument in your text?  How do you back up your claim?  What evidence do you use to back up your argument? 

#### 3) Method of analysis  

A method of analysis refers to the analytical methods you will use to gather and interpret your evidence and support your claim.  It is important here that you make a connection between what theory, method, and data you use and why it is useful for supporting your main claim.   

Which method of analysis will you use for your research question and backing up your main claim?  Which theoretical perspective will use in your assignment?   

#### 4) Limitations  

In the limitations section you should analyze the limitations of your chosen method of analysis. This involves discussing the weaknesses of your method(s), and what impact these weaknesses may have on your work.  
 
What weaknesses do you see with the methods? What problems might you encounter if you use them? 

#### 5) Backing up 

This is where you back up and support the method of analysis you have chosen.  This can be done in a few different ways, including explaining how other researchers have used this method, or by drawing on texts and other established sources that support your chosen method.  

What are the strengths of your method of analysis? Why did you choose it, despite the limitations it poses for your main claim? 

#### 6) Strength of argument  

In the strength of argument section, you address the degree of certainty with which you are able to make your main claim.  This is done by discussing the different factors that might impact your main claim, and any other reservations you may have as.   

To what extent are you certain of your claim, its possibilities, or probabilities? 

 

::: oppgave Exercise: Analyze one of your texts 

Use these questions to analyze your text 

1) What is the main claim? 

2) What is your argument, and what documents and evidence will you use to support it? 

3) Which method of analysis will you use? 

4) What are the limitations of your chosen method of analysis? 

5) What support is there of your chosen method? 

6) To what extent are you certain of your claim, its possibilities, or probabilities? 

:::
