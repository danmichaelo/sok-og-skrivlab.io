---
title: "Writing"
date: "2012-03-16"
prev: "../study-skills/academic-genres"
---

# Writing

Academic writing has certain formal requirements. In order to write a good thesis, you must satisfy academic standards with regard to language, style, structure, and to the content of your thesis. Another important aspect is of course the correct use of [sources.](/en/sources-and-referencing/how-to-cite.html)

<Figure
  src="/images/illustrasjoner_skriving_500x450.png"
  alt="Birds eye view of person writing on a laptop"
  caption=""
  type="right"
/>

This section contains advice on writing an academic bachelor's or master's thesis. We give you a thorough introduction to structuring longer texts, and advice on academic language and style. You will find tips on getting started with your writing, and examples of ways to cope with the different phases of writing; writing for yourself, presenting text to others, and the completion of your thesis.

::: Warning Different requirements
Remember that different requirements apply in different fields and institutions. Always check your own field’s particular requirements in addition to the general advice offered here.
:::
