---
home: true
# heroImage: /sos-logo.svg
# actionText:  La oss starte →
# actionLink: /soking/
footer: Søk & Skriv - Creative Commons Attribution-NonCommercial-ShareAlike 4.0
---

<div class="cards">
  <div class="card">
    <div class="image">
      <a href="/soking/"><img src="/images/illustrasjoner_sok_500x450.png" alt="Søking"></a>
    </div>
    <div class="content">
      <h2><a href="/soking/">Søking</a></h2>
      <p>Få hjelp til å søke etter informasjon og komme i gang med oppgaveskriving.</p>
    </div>
  </div>

  <div class="card">
    <div class="image">
      <a href="/studieteknikk/"><img src="/images/illustrasjoner_lesing_500x450.png" alt="Studieteknikk"></a>
    </div>
    <div class="content">
      <h2><a href="/studieteknikk/">Studieteknikk</a></h2>
      <p>Her får du tips om hvordan du kan lese og forstå tekster, ta gode notater, samarbeide og jobbe i studiegrupper.</p>
    </div>
  </div>

  <div class="card">
    <div class="image">
      <a href="/skriving/"><img src="/images/illustrasjoner_skriving_500x450.png" alt="Skriving"></a>
    </div>
    <div class="content">
      <h2><a href="/skriving/">Skriving</a></h2>
      <p>I akademisk skriving stilles det bestemte krav til form og innhold, språk og stil.</p>
    </div>
  </div>

  <div class="card">
    <div class="image">
      <a href="/kjeldebruk/"><img src="/images/illustrasjoner_kildehenvisning_500x450.png" alt="Kjeldebruk"></a>
    </div>
    <div class="content">
      <h2><a href="/kjeldebruk/">Kjeldebruk</a></h2>
      <p>All forsking bygger på kjelder. Det finst fleire vilkår for å vurdere relevans og kvalitet.</p>
    </div>
  </div>
</div>

---

<div class="card references">
  <!-- <div class="image">
    <a href="/referansestiler"><img src="/images/illustrasjoner_lesing_500x450.png" alt="Referansestiler"></a>
  </div> -->
  <div class="content">
    <span class="tags">
      <span class="tag"><a href="/referansestiler/apa-6th.html">APA 6th</a></span>
      <span class="tag"><a href="/referansestiler/apa-7th.html">APA 7th</a></span>
      <span class="tag"><a href="/referansestiler/chicago-forfatter-aar.html">Chicago forfatter-år</a></span>
      <span class="tag"><a href="/referansestiler/chicago-fotnoter.html">Chicago fotnoter</a></span>
      <span class="tag"><a href="/referansestiler/harvard.html">Harvard</a></span>
      <span class="tag"><a href="/referansestiler/mla.html">MLA</a></span>
      <span class="tag"><a href="/referansestiler/vancouver.html">Vancouver</a></span>
    </span>
  </div>
</div>

## Siste videoer

<div class="container">
  <div class="video">
    <Video id="3IIoBZ0Tf_I" />
  </div>
  <div class="video">
    <Video id="LswBxnztpzU" />
  </div>
</div>

[Se flere](/video/soking.html)

## Partnere

<div class="partners">
  <div class="partner">
    <a href="https://www.hvl.no">
      <picture>
        <source media="(max-width: 719px)" srcset="/partners/hvl-icon.jpg">
        <img src="/partners/hvl-logo.jpg" alt="Høyskolen på Vestlandet"/>
      </picture>
    </a>
    <div class="title">
      Høgskulen på Vestlandet
    </div>
  </div>
  <div class="partner">
    <a href="https://www.uib.no">
      <picture>
        <source media="(max-width: 719px)" srcset="/partners/uib-icon.png">
        <img src="/partners/uib-logo.png" alt="Universitetet i Bergen"/>
      </picture>
    </a>
    <div class="title">
      Universitetet i Bergen
    </div>
  </div>
  <div class="partner">
    <a href="https://www.uio.no">
      <picture>
        <source media="(max-width: 719px)" srcset="/partners/uio-icon.png">
        <img src="/partners/uio-logo.png" alt="Universitetet i Oslo"/>
      </picture>
    </a>
    <div class="title">
      Universitetet i Oslo
    </div>
  </div>
  <div class="partner">
    <a href="https://www.nb.no">
      <picture>
        <source media="(max-width: 719px)" srcset="/partners/nb-icon.png">
        <img src="/partners/nb-logo.png" alt="Nasjonalbiblioteket"/>
      </picture>
    </a>
    <div class="title">
      Nasjonalbiblioteket
    </div>
  </div>
</div>

--- 

<div class="container two-column footer-links">
  <div class="align-right">
    <div><a href="/om/">Om Søk & Skriv</a></div>
    <div><a href="/om/kontaktinformasjon.html">Kontaktinformasjon</a></div>
    <div><a href="/om/sok-og-skriv-i-undervisning.html">Søk & Skriv i undervisning</a></div>
  </div>
  <div class="align-left">
    <div><a href="/en/about/">About Search & Write</a></div>
    <div><a href="/en/about/contact-information.html">Contact</a></div>
  </div>
</div>
