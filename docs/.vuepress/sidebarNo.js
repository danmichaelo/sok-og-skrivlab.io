module.exports = {
  '/soking/': [
    '',
    'planlegg-soket-ditt',
    'soketeknikker',
    'systematisk-soking',
  ],
  '/studieteknikk/': [
    '',
    'lesemater',
    'lesing-og-skriving',
    'studiegrupper',
    'argumentasjon-i-tekst',
    'akademiske-sjangrer'
  ],
  '/skriving/': [
    '',
    'kom-i-gang-med-a-skrive',
    'struktur',
    'argumentere-redegjore-drofte',
    'oppbygning-av-en-oppgave',
    'imrad-modellen',
    'akademisk-sprak-og-stil',
    'formelle-krav-til-oppsett'
  ],
  '/kjeldebruk/': [
    '',
    'korleis-skal-ein-referere',
    'ulike-kjelder',
    'kjeldevurdering',
   ],
  '/referansestiler/': [
    '',
    'apa-6th',
    'apa-7th',
    'chicago-forfatter-aar',
    'chicago-fotnoter',
    'harvard',
    'mla',
    'vancouver'
  ],
  '/video/': [
    'soking',
    'studieteknikk',
    'skriving',
    'kjeldebruk'
  ],
  '/om/brukertest/': [
    '',
    'eksempel2',
    'eksempel3',
    'eksempel4',
    'eksempel5'
  ],
  '/': [
    '/om/',
    '/om/kontaktinformasjon',
    '/om/sok-og-skriv-i-undervisning'
  ]
}
