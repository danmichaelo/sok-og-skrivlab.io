---
title: 📺 Kjeldebruk
---

# Kjeldebruk

## Hvordan sette inn referanser og unngå plagiat? 

<Video id="3IIoBZ0Tf_I" />


## Kildekritikk

<Video id="LswBxnztpzU" />


## Hvordan finne noe interessant å skrive oppgave om?

Anders Johansen er professor ved Institutt for informasjons- og medievitenskap ved UiB. Her snakker han om hvordan finne en interessant problemstilling for en bachelor- eller masteroppgave.

<Video id="FOwhrXNnHNI" />


## Hvordan bruke informanter som kilde i akademisk tekst?

Odd Are Berkaak, professor i sosialantropologi ved Universitetet i Oslo, forteller om hvordan en kan bruke feltmateriale samlet inn ved deltagende observasjon i en akademisk tekst.

<Video id="ktM1r1Ca5Tc" />


## Kva er skilnaden på ei god og ei dårleg oppgåve?

Lars Nyre er professor ved Institutt for informasjons- og medievitskap ved UiB. Her fortel han om ei oppgåve han skreiv som ikkje blei like godt motteken som han hadde håpa.

<Video id="GD1scK6R01A" />


## Kva kjenneteiknar god akademisk kjeldebruk?

Ole Bjørn Rekdal, professor ved Institutt for velferd og deltaking ved Høgskulen på Vestlandet, fortel om kor viktig det er med god kjeldebruk i akademisk skriving.

<Video id="GPR0phJIsuk" />

