---
title: "APA 7th"
date: "2020-06-17"
---

# APA 7th

APA står for American Psychological Association. Foreningen har utviklet en typisk forfatter-årstallstil som er vanlig innenfor blant annet psykologi, økonomi, pedagogikk, helsefag og realfag. Anbefalingene som gis her er resultat av et [nasjonalt samarbeid](https://www.unit.no/tjenester/norsk-apa-referansestil). Skriver du på engelsk, eller har fått beskjed om å bruke en engelsk manual, anbefaler vi å sjekke [APA-bloggen](https://apastyle.apa.org/style-grammar-guidelines/references/examples).

## Henvisninger i teksten 

I teksten oppgis kildens forfatter(e), årstall og eventuelt sidetall. Henvisning til flere kilder i samme parentes skilles med semikolon. Merk at &-tegnet kun brukes i parenteser og referanseliste. I teksten brukes «og».

Lengre, direkte sitater (40 ord eller flere) skal settes opp med innrykket venstremarg, ekstra linjeskift over og under og uten anførselstegn. Punktum settes da _foran_ henvisningen i slutten av sitatet. Les mer om [sitering og kildehenvisninger](https://sokogskriv.no/kjeldebruk/korleis-skal-ein-referere.html#sitat).

::: tip Eksempel med flere kilder  

Uttrykket «bærekraftig utvikling» kan ha mange ulike betydninger (Hume & Barry, 2015; Lafferty & Langhelle, 1995; Sterling, 2009). 

::: 

::: tip Antall forfattere

**En til to forfattere:**

(Etternavn, årstall, evt. sidetall)

… (Sterling, 2009, s. 10)

(Etternavn & Etternavn, årstall, evt. sidetall)

… (Gullestad & Killingmo, 2013, s. 35)

**Tre eller flere forfattere:**

(Første forfatters etternavn et al., årstall, evt. sidetall)

… (Adam et al., 2017, s. 99)

:::

## Referanselisten

Med unntak for enkelte offentlige dokumenter, består en referanse i APA 7th av fire deler i fast rekkefølge: forfatter, publiseringstidspunkt, verktittel og verkets kilde. Disse gir svar på spørsmålene om _hvem_ som er ansvarlig for verket, _når_ verket er publisert, _hva_ verket heter og _hvor_ det kan finnes.

Oppsettet varierer etter dokumenttype. Selvstendige utgivelser som bøker, rapporter, filmer, TV-serier og kunstverk skal ha tittel i kursiv, mens dokumenter som inngår i et større hele, som kapitler og tidsskriftartikler ikke skal ha det. Bøker og rapporter kan noen ganger ha en parentes like etter tittelen som oppgir utgave, bind eller sidetall (for enkeltkapitler) eller serienummer for rapporter og retningslinjer. Mer uvanlige referanser, som bilder, brosjyrer eller LP-plater, kan ha en hakeparentes samme sted som angir verkets format.

For selvstendige dokumenter vil referansens _hvor_ være utgiver for fysiske utgivelser, og DOI-nummer eller URL for elektroniske publikasjoner. For dokumenter som inngår i en større utgivelse, som et tidsskrift eller en redigert bok, vil denne inngå i referansens _hvor_. Variasjonen her er stor, og mer inngående beskrevet i eksempelsamlingen under.

- Referanselisten skal ordnes alfabetisk på forfatternes etternavn.
- Har en forfatter skrevet flere verk, skal de ordnes kronologisk med eldste årstall først.
- Har en forfatter skrevet flere verk i samme år brukes a, b, c, etter årstallet for å skille arbeidene: 2018a, 2018b osv.
- Ved referanser som skrives over flere linjer skal alle linjene ha innrykk unntatt den første (hengende innrykk).

::: tip Antall forfattere

**En til to forfattere**

_Alle forfatterne listes opp_ 

<span class="ref">Gilje, N. & Grimen, H. (1993). Samfunnsvitenskapenes forutsetninger: Innføring i samfunnsvitenskapenes vitenskapsfilosofi. Universitetsforlaget.</span> 

**Tre til 20 forfattere:**

_I referanselista skal navnet til alle forfatterne være med_

<span class="ref">Løvvik, O. M., Rauwel, P. & Prytz, Ø. (2011). Self-diffusion in Zn4Sb3 from first-principles molecular dynamics. Computational Materials Science,50(9), 2663–2665. [https://doi.org/10.1016/j.commatsci.2011.04.015](https://doi.org/10.1016/j.commatsci.2011.04.015)</span>


**21 eller flere forfattere:**

_I referanselista tar du med de 19 første forfatterne etterfulgt av utelatelsestegn (. . .) og deretter siste forfatter_

<span class="ref">Adam, J., Adamová, D., Aggarwal, M. M., Aglieri Rinella, G., Agnello, M., Agrawal, N., Ahammed, Z., Ahmad, S., Ahn, S. U., Aiola, S., Akindinov, A., Alam, S. N., Albuquerque, D. S. D., Aleksandrov, D., Alessandro, B., Alexandre, D., Alfaro Molina, R., Alici, A., Alkin, A., . . . Garg, K. (2017). Determination of the event collision time with the ALICE detector at the LHC. The European Physical Journal Plus, 132(2), Artikkel 99. [https://doi.org/10.1140/epjp/i2017-11279-1](https://doi.org/10.1140/epjp/i2017-11279-1)</span>

:::

## Eksempler

::: warning Flere eksempler
For andre eksempler enn de listet opp nedenfor, sjekk APA-manualen som du finner på [denne sida](https://www.unit.no/tjenester/norsk-apa-referansestil).
:::

### Bok

::: teksten

... (Forfatter, årstall, evt. sitedall)

... (Gullestad & Killingmo, 2013, s. 35)

:::

:::: ref

  
<span class="ref"> Forfatter, A. A., Forfatter, B. B. & Forfatter, C. C. (utgivelsesår). _Tittel: Undertittel_ (evt. utgave). Forlag. </span>

<span class="ref">Gullestad, S. E., & Killingmo, B. (2013). _Underteksten: psykoanalytisk terapi i praksis_ (2. utg.). Universitetsforlaget.</span>

Utgaveopplysninger skal ikke føres opp ved førsteutgaver.


::: details I EndNote

Legges inn som "Book".

Fyll ut feltene: Author (trykk "enter" mellom hvert navn hvis flere), Year, Title, Publisher, Edition (hvis aktuelt) og Translator (hvis aktuelt).
::: 
:::: 

::: details Flere eksempler
De seks medisinske kjernespørsmålene (Nortvedt et al., 2012, s. 35) bestemmer hvilken ... 

<span class="ref">Nortvedt, M.W., Jamtvedt, G., Graverholt, B., Nordheim, L.V. & Reinar, L.M. (2012). _Jobb kunnskapsbasert! En arbeidsbok_ (2. utg.) Akribe.</span>

For bøker med inntil 20 forfattere oppgis alle forfatternavn i referanselisten. For bøker med 21 eller forfattere nevnes de fire første og den siste med en ellipse imellom, slik: Forfatter, A. A., Forfatter, B. B., Forfatter, C. C., Forfatter, D. D., ... Forfatter, Ø. Ø.
::: 


### Kapittel i redigert bok

Det er mest vanlig å referere til enkelte kapittel i redigerte bøker. Dersom du skal referere til hele boken bruker du malen for bok.

::: teksten
(Kapittelforfatter, årstall, evt. sidetall)

En større undersøkelse i Australia viste at ... (Trickett, 2001, s. 9646).
:::

:::: ref
<span class="ref">Kapittelforfatter, A. A. (Årstall). Kapitteltittel. I A. Redaktør & B. Redaktør (Red.), _Tittel_ (s. xxx–xxx). Forlag.</span>

<span class="ref">Trickett, E. J. (2001). Mental Health: Community Interventions. I N. J. Smelser & P. B. Baltes (Red.), _International encyclopedia of the social & the behavioral sciences_ (s. 9645–9648). Elsevier.</span>

::: details I EndNote

Legges inn som "Book Section".

Fyll ut feltene: Author (trykk "enter" mellom hvert navn hvis flere), Year, Title, Editor (trykk "enter" mellom hvert navn hvis flere), Book Title, Publisher, Pages, Edition (hvis aktuelt). 
::: 
:::: 

### E-Bok

Dersom boken har DOI-nummer, foretrekkes dette. Hvis e-boken identisk med trykt utgave, er URL/DOI valgfritt. Dersom boken ikke har DOI, og e-bokversjonen kun er tilgjengelig via en betalingstjeneste, refererer du til boken som trykt versjon. 

::: teksten
(Forfatter, årstall, eventuelt sidetall)

#### Eksempler: 

1. ... (Vareide, 2018, s. 166).
2. Vareide (2018, s. 166) hevder at ...

:::

:::: ref
<span class="ref">Forfatter, A. A. & Forfatter, B. B. (Årstall). _Tittel: Undertittel_ (utgave)\[evt. versjon, f.eks. Kindle\]. https://doi.org/xxxx</span>

<span class="ref">Vareide, K. (2018) _Hvorfor vokser steder? Og hvordan kan utviklingen påvirkes?_ Cappelen Damm. https://doi.org/10.23865/noasp.32</span>

::: details I EndNote

Legges inn som "Electronic Book".

E-bok med DOI: Fyll ut feltene Author (trykk "enter" mellom hvert navn hvis flere), Year, Title, DOI.

E-bok uten DOI: Fyll ut feltene Author, Year, Title, Publisher, URL.

(Bruk feltet Type of Medium dersom e-boka er begrenset til lesing på et spesielt lesebrett). 
::: 
:::: 


### Religiøse og klassiske verk

Religiøse verk (f.eks. Bibelen, Koranen), klassiske verk (greske og romerske verk) og klassisk litteratur (f.eks. Shakespeare) refereres som for bøker. Religiøse verk behandles som verk uten forfatter, mens annoterte versjoner (kommentarutgaver) behandles som redigert bok. Religiøse skrifter er publisert i mange utgaver; bruk årstallet for den aktuelle utgivelsen. Dersom publiseringstidspunkt anses å være kjent, oppgis årstall for første utgivelse i opprinnelseslandet i tillegg til den utgaven du har brukt. Siden sidetall vil variere fra utgave til utgave, brukes nummererte kapittel, vers eller surer i henvisningen. Bibelen og andre hellige verk skal generelt ikke ha innførsel i referanselista, men hvis du mener at det er nyttig for leseren kan den inkluderes.

::: teksten
... «for det var ikke husrom for dem» (Luk 2,7, Bibel 2011). 
Deretter kun henvisning til del, kapittel, vers, for eksempel (Luk 2,7).

(Koranen 1: 6–7)
\[Ingen innførsel\]

I Lukasevangeliet (Luk,1:34) er Maria i tvil om hun virkelig vil føde Guds barn.
\[Ingen innførsel\]

I Ciceros «Om taleren» (1.47, overs. 1993) ...
\[Ingen innførsel\]

I _Iliaden_ (6.344–58) sier Helena at ...
\[Ingen innførsel\]

... (Dante Alighieri, 1472/2005, s. 241).

... (Homer,ca 800 fvt./1898).
:::

:::: ref
<span class="ref">Dante Alighieri. (2005). _Den guddommelege komedie_ (2. utg., M. Ulleland, Overs.). Gyldendal. (Opprinnelig utgitt 1472)</span>

<span class="ref">Homer (1898). _The Iliad_ (S. Butler, Overs.) The Internet Classics Archive. http://classics.mit.edu/Homer/iliad.html (Originalt verk publisert ca. 800 fvt.)</span>

::: details I EndNote

Legges inn som "Book".
Fyll ut feltene: Author (trykk "enter" mellom hvert navn hvis flere), Year, Title,  Publisher, Translator. Opplysninger om originalutgave legges inn manuelt. (Gjøres når teksten er ferdig ved å trykke "convert to plain text").
NB: Hellige verk skal generelt _ikke_ ha innførsel i referanselista. Skriv henvisning i teksten manuelt.

::: 
:::: 

### Oversatte bøker
Leser du et verk som er oversatt, er det oversettelsen det refereres til og ikke originalverket. Oversetteren skal krediteres i referansen. Årstallet for originalverket oppgis i slutten av referansen: 

::: ref
<span class="ref">Tolkien, J. R. R. (2004). _Ringenes herre: Ringens brorskap_ (T. B. Høverstad, Overs.). Tiden. (Opprinnelig utgitt 1954).</span>


::: details I EndNote

Legges inn som "Book".
Fyll ut feltene: Author (trykk "enter" mellom hvert navn hvis flere), Year, Title,  Publisher, Translator. Opplysninger om originalutgave legges inn manuelt. (Gjøres når teksten er ferdig ved å trykke "convert to plain text").

::: 
:::

### Verk utgitt på nytt
Et gjenutgitt verk er et verk som har gått ut av trykk (eller ikke lenger er tilgjengelig) og siden publisert på nytt, noe er som vanlig for eldre verk. Når du henviser til et gjenutgitt verk (f.eks. et verk gjenutgitt i form av en tekstsamling eller antologi), før opp informasjonen om den nye publikasjonen som du har brukt, etterfulgt av det opprinnelige utgivelsesåret i slutten av referansen

:::ref
<span class="ref">... (x/1916) ... (Opprinnelig utgitt 1916).</span>


::: details I EndNote


Legges inn som "Book".
Fyll ut feltene: Author (trykk "enter" mellom hvert navn hvis flere), Year, Title,  Publisher, Translator. Opplysninger om originalutgave legges inn manuelt. (Gjøres når teksten er ferdig ved å trykke "convert to plain text").

::: 
:::

## Artikler 

### Artikkel i tidsskrift (trykt og elektronisk)

::: teksten

(Forfatter(-e), årstall, eventuelt sidetall)

... langtidsvirkninger av vold (Huang & Mossige, 2012, s. 150).

:::

:::: ref
<span class="ref">Forfatter, A. A., Forfatter, B. B. & Forfatter, C. C. (Årstall). Artikkeltittel. _Tidsskrifttittel_, _årgang/volum_(hefte/issue), sidespenn.</span>

<span class="ref">Huang, L. & Mossige, S. (2012). Academic achievement in Norwegian secondary schools: The impact of violence during childhood. _Social Psychology of Education_, _15_(2), 147–164. https://doi.org/10.1007/s11218-011-9174-y</span>

::: details I EndNote

Trykt tidsskrift: Legges inn som "Journal Article". 

Elektronisk tidsskrift: Legges inn som "Electronic Article" eller "Journal Article".

Trykt tidsskrift: Fyll ut feltene Author (trykk "enter" mellom hvert navn hvis flere), Year, Title, Journal, Volume, Issue (hvis aktuelt), Pages.

Elektronisk tidsskrift: Fyll ut feltene Author, Year, Title, Journal, Volume, Issue (hvis aktuelt), Pages, DOI (eller URL dersom DOI mangler).
::: 
:::: 

Oppgi DOI dersom det foreligger. Hvis kildeinformasjon som årgang/volum, hefte/issue eller sidetall ikke er oppgitt, utelates disse fra referansen. 



### Artikkel i avis (trykt og elektronisk)

::: teksten
(Forfatter, årstall, eventuelt sidetall)

Man må se mer nyansert på utroskap og monogami (Thuen, 2006, s. 12).

(Knapstad, 2018).
:::

:::: ref
<span class="ref">Forfatter. (Årstall, dato). Tittel på artikkel: undetittel. _Avisens tittel_, side. URL om du leser avisartikkelen elektronisk. </span>

<span class="ref">Thuen, F. (2006, 12. oktober). Utroskapens pris. _Bergens Tidende_, s. 12.</span>

<span class="ref">Knapstad, M. L. (2018, 9. mai). En jafs av Bergen. Aftenposten, A-magasinet. https://www.aftenposten.no/a-magasinet/i/ngJron/en-jafs-av-bergen?</span>

::: details I EndNote

Legges inn som "Newspaper Article".

Fyll ut feltene: Reporter (trykk "enter" mellom hvert navn hvis flere), Year, Title, Newspaper, Pages, Issue Date og url om du leser avisartikkelen elektronisk.
::: 
:::: 

### Artikler fra leksikon/oppslagsverk 

::: teksten 
(forfatter, år)

(Fossheim, 2018).

Om artikkelen ikke har forfatter:
("artikkelens tittel")

... ("Lindeberg stasjon", 2017).

... (10.utg.; ICD-10; World Health Organization, 2020).
:::


:::: ref
Forfatter. (oppdaterings- eller publiseringstidspunkt). Tittel: eventuelt Undertittel. 
    Nettstedets navn. https://xxxxx

<span class="ref">Fossheim, H. (2018, 25. desember). Aristoteles. 
   I _Store Norske Leksikon_. https://snl.no/Aristoteles</span>

Om artikkelen ikke har forfatter: 

<span class="ref"> Lindeberg stasjon. (2017, 8.mars). I _Wikipedia_. 
    https://no.wikipedia.org/w/index.php?title=Lindeberg_stasjon&oldid=17219238</span>

<span class="ref">World Health Organization. (2020). F40: Fobiske angstlidelser. I Den                      internasjonale statiske klassifikasjonen av sykdommer og beslektede sykdommer(10. utg.). Direktoratet for e-helse. https://finnkode.ehelse.no/#icd10/0/0/0/2599502</span>

::: details I Endnote

Legges inn som "Encyclopedia".

Fyll ut feltene: Author (hvis innførselen har forfatter), Year, Title, Editor (hvis aktuelt), Encyclopedia Title, Edition (hvis aktuelt), Pages (hvis aktuelt), Date (hvis publiseringsdato er oppgitt i tillegg til publiseringsår), URL.

(Fyll også ut feltet Access Date hvis lesedato skal med).
:::
::::


## Nettsider

### Nettside fra organisasjon

::: teksten
(Organisasjon, årstall)

... som det ble lagt fram i statsråd (Statsministerens kontor, 2018).
:::

:::: ref
<span class="ref">Organisasjon. (Årstall, dag, måned). _Tittel_. Nettstedets navn.  URL.</span>

<span class="ref">Statsministerens kontor. (2018, 20. mars). _Offisielt fra
statsråd 20. mars 2018_. Regjeringen.
https://www.regjeringen.no/no/aktuelt/offisielt-fra-statsrad-20.-mars-2018/id2594402/
</span>

::: details I EndNote


Legges inn som "Web Page".

Fyll ut feltene: Author (med et komma etter organisasjonsnavnet eller institusjonsnavnet), Year, Title, Publisher (Nettstedets navn) Last Update Date (hvis publiseringsdato er oppgitt i tillegg til publiseringsår), URL.

(Fyll også ut Access Year og Access Date hvis lesedato skal med). 
::: 
:::: 

#### Merknader 
Nettstedets navn utgår dersom det er det samme som forfatter/organisasjonen. 

Publiseringsdato brukes _kun_ for innhold som publiseres eller oppdateres ofte (f.eks. blogginnlegg, diskusjonsfora, sosiale medieoppdateringer, pressemeldinger, nyhetsbrev, høringsuttalelser). I teksten oppgis bare årstallet for utgivelsen. Finnes ikke det ikke informasjon om publiseringstidspunkt, oppgis u.å. (= uten år) i referansen og i teksten.  


### Nettside med forfatter

::: teksten
(Forfatter, årstall)

Skinnegående transport øker mest (Lund, 2018).
:::

:::: ref
<span class="ref">Forfatter, A.A. (Årstall, dato). _Tittel_. Nettstedets navn. URL</span>

<span class="ref">Lund, V. (2018, 26. september). _Åtte av ti reiser med bil_. SSB. https://www.ssb.no/transport-og-reiseliv/artikler-og-publikasjoner/atte-av-ti-reiser-med-bil </span>

::: details I EndNote

Legges inn som "Web Page".

Fyll ut feltene: Author (trykk "enter" mellom hvert navn hvis flere), Year, Title, Publisher (Nettstedets navn) Last Update Date (hvis publiseringsdato er oppgitt i tillegg til publiseringsår), URL.

:::
:::: 


## Offentlige kilder

### Lover

::: teksten
(Lovens kortnavn, årstall, paragraf)

I folketrygdloven (1997, § 14-4) står det om svangerskapspenger at ...
:::

:::: ref
<span class="ref">Lovens kortnavn. (Årstall). _Lang tittel_ (datokode). Nettstedets navn. https://...</span>

<span class="ref">Folketrygdloven. (1997). _Lov om folketrygd_ (LOV-1997-02-28-19). Lovdata. [https://lovdata.no/lov/1997-02-28-19](https://lovdata.no/lov/1997-02-28-19)</span>

::: details I EndNote

(tilpasset EndNote-stil for norsk APA)

Legges inn som "Legal Rule or Regulation".

Fyll ut feltene: Author (lovens korttittel), Year, Title (full tittel), Rule Number (datokode), Publisher, URL.
::: 
:::: 

::: details Flere eksempler
Med endringslov til folketrygdloven (2018) endres finansieringen ...

<span class="ref">Endringslov til folketrygdloven. (2018). _Lov om endring i folketrygdloven_ (LOV-2018-06-22-55). Lovdata. [https://lovdata.no/lov/2018-06-22-55](https://lovdata.no/lov/2018-06-22-55)</span>
:::

#### Merknader
Det anbefales å gå direkte til Lovdata for lovene. Om du bruker lovene i f.eks bokformat henvises det som for bok. 

### Forskrifter

::: teksten
(Forskriftens kortnavn, årstall, paragraf)

Forskrift om rammeplan for grunnskolelærerutdanning, trinn 1–7 (2016), sier at ...
:::

:::: ref
<span class="ref">Forskriftens kortform. (årstall). _Lang tittel_ (datokode). Nettstedets navn. https://...</span>

<span class="ref">Forskrift om plan for grunnskolelærerutdanning, trinn 1–7. (2016). _Forskrift om rammeplan for grunnskolelærerutdanning for trinn 1–7_ (FOR-2016-06-07-860). Lovdata. [https://lovdata.no/forskrift/2016-06-07-860](https://lovdata.no/forskrift/2016-06-07-860)</span>

::: details I EndNote


Fyll ut feltene: Author (Forskriftens kortform), Year, Title (full tittel), Rule Number (datokode), URL.
::: 
:::: 

### Dommer
Som regel holder det å henvise til dommer kun i teksten. Ulike fagtradisjoner har ulike praksiser. Undersøk med faglærer eller veileder om du trenger mer hjelp.

::: teksten
Problemstillingen er berørt i dommen (Rt 1993 s. 766).
:::

::: ref
_ingen innførsel_
:::

### Meldinger og proposisjoner til Stortinget

::: teksten
(Meldingsnummer (år-år), eventuelt sidetall)

Sammenslåingene ble begrunnet med noe de kalte «en kritisk vurdering av kvalitet» (Meld. St.20 (2009-2010), s. 49).
:::

:::: ref
<span class="ref">Meldingsnummer (år-år). _Tittel: Undertittel_. Departement. URL</span>

Dersom du henviser til trykt utgave, utgår url. 

<span class="ref">Meld. St. 20 (2009-2010). _Omorganisering av ABM-utvikling_. Kulturdepartementet. https://www.regjeringen.no/no/dokumenter/Meld-St-20-20092010/id608202/</span>


::: details I EndNote

Legges inn som "Government Document".

Fyll ut feltene: Author (Sett inn årstall i parentes etter meldingsnummer og avslutt med komma), Title, Department, URL.

OBS: Author-feltet skal altså se slik ut: Meld.St. 20 (2009-2010),

::: 
:::: 

::: details Flere eksempler
De funksjonshemmedes stilling blir behandlet i handlingsplanen (St.Meld. nr.8(1998-1999)).


<span class="ref"> St.meld. nr. 8 (1998–99). _Om handlingsplan for funksjonshemma 1998–2001: Deltaking og likestilling_. Sosial- og helsedepartementet.</span>

... (Arbeids-og sosialkomiteen, 2016)

<span class="ref">Innst. 49 S (2016–2017). Innstilling fra arbeids-og sosialkomiteen om Nav i en ny tid – for arbeid og aktivitet. Arbeids- og sosialkomiteen. https://www.stortinget.no/no/Saker-og-publikasjoner/Publikasjoner/Innstillinger/Stortinget/2016-2017/inns-201617-049s/</span> 

:::

#### Merknader
Meldinger fra før oktober 2009 har betegnelsen Stortingsmelding, forkortet St.meld., mens nyere meldinger heter Melding til Stortinget, forkortet Meld. St.

### Norges offentlige utredninger (NOU)

::: teksten
(NOU årstall: nummer, eventuelt sidetall)

... i NOU 1999: 13 (s. 559).
:::

:::: ref
<span class="ref">NOU årstall: nummer. (Utgivelsesår). _Tittel_. Departement.</span>


<span class="ref">NOU 1999: 13. (1999). _Kvinners helse i Norge_. Sosial- og helsedepartementet.</span>

::: details I EndNote

Trykt utgave: Legges inn som "Book".

Elektronisk utgave: Legges inn som "Electronic Book".

Trykt utgave: Fyll ut feltene Author (legg inn NOU år: nummer som forfatter med et komma til slutt), Year, Title, Publisher.

Elektronisk utgave: Fyll ut feltene Author (legg inn NOU år: nummer som forfatter med et komma til slutt), Year, Title, URL.

OBS: År for utgave skal ikke med i parentesen i løpende tekst. Dette må rettes manuelt i EndNote via "Edit & Manage Citations". Velg Exclude year i nedtrekksmenyen for "Formatting".
::: 
:::: 

::: details  Flere eksempler
... behandling av hjerteinfarkt (NOU 1999: 13, 1999, s. 559).

<span class="ref">NOU 1999: 13. (1999). _Kvinners helse i Norge_. Sosial- og helsedepartementet. https://www.regjeringen.no/</span>
:::

### Ramme- og læreplaner

::: teksten
(Organisasjon, årstall, eventuelt sidetall)

... (Kunnskapsdepartementet, 2017, s. 7).
... (Utdanningsdirektoratet, 2020)
... (Kunnskapsdepartementet, 2017, Demokrati) 

:::

:::: ref
<span class="ref">Organisasjon. (årstall). _Tittel_ (kode). Nettstedets navn. URL </span>

Kunnskapsdepartementet bør stå som forfatter av Kunnskapsløftets overordnede del, mens Utdanningsdirektoratet som forvaltningsorgan betraktes som forfatter av læreplaner for fag.

<span class="ref">Kunnskapsdepartementet. (2017). _Overordnet del – verdier og prinsipper_. Regjeringen. https://www.regjeringen.no/</span>
<span class="ref">Utdanningsdirektoratet. (2020). _Læreplan i naturfag (NAT01-04)_. https://www.udir.no/lk20/nat01-04</span>
<span class="ref">Kunnskapsdepartementet. (2017). _Rammeplan for barnehagen: Forskrift om rammeplan for barnehagens innhold og oppgaver_. Udir. https://www.udir.no/globalassets/filer/barnehage/rammeplan/rammeplan-for-barnehagen-bokmal2017.pdf</span>


::: details I EndNote

Legges inn som "Report" eller "Government Document".

Fyll ut feltene: Author, (komma etter organisasjonsnavn) Year, Title, Report Number (plankode), URL. 
::: 
:::: 

### Rapporter

::: teksten
(Forfatter, årstall, eventuelt sidetall)

(Nordisk ministerråd, 2009)
:::

:::: ref
Elektronisk:
<span class="ref">Forfatter/Institusjon. (Publiseringstidspunkt). _Tittel_ (seriekode/Rapportnavn og -nr).
https://xxxxx


Trykt:
<span class="ref">Forfatter/Institusjon. (publiseringstidspunkt). _Tittel_ (seriekode/Rapportnavn og -nr). Utgiver.</span>

<span class="ref">Bergan, M. A. (2017). _Fiskebiologiske undersøkelser i
Balsnesvassdraget på Ørland i 2017:
Ungfisktellinger og problemkartlegging knyttet til
fiskeforsterkende tiltak og sjøørret_ (NINA Rapport
1392). http://hdl.handle.net/11250/2485755</span>

:::: 

::: details Flere eksempler
...(Hansen & Slagsvold, 2013, s. 8)

<span class="ref">Hansen, T. & Slagsvold, B. (2012). _Likestilling hjemme_ (NOVA Rapport 8/12). Hentet fra https://www.nova.no/</span>
:::

::: details I EndNote

Legges inn som "Report"

Fyll ut feltene: Author (trykk "enter" mellom hvert navn hvis flere), Year, Title, Report number (Her skriver du inn hele serieopplysningen),Publisher, URL

Merknad: Dersom du ikke har URL, erstattes den med Publisher.

:::




## Andre kildetyper

### Kompendier

Dersom man skal referere til tekster fra et kompendium, skal man henvise til originalverket (for eksempel tidsskriftet eller boken), ikke til kompendiet. Dette gjelder også sidehenvisninger.

### Studentoppgaver og doktorgradsavhandlinger 


::: teksten
(Forfatter, årstall, eventuelt sidetall)

... (Våpenstad, 2011, s. 59).

Kyrkjebø (2001) viser forskjellen mellom ... (s. 39).
:::

:::: ref
Trykt oppgave:  
<span class="ref">Forfatter, A. A. (Årstall). _Tittel: Undertittel_/[Masteroppgave/Doktorgradsavhandling/. Institusjon].</span>

Avhandling på nett:  
<span class="ref">Forfatter, A. A. (Årstall). _Tittel:Undertittel_/[Masteroppgave/Doktorgradsavhandling, Institusjon/]. Navn på arkiv .</span>

<span class="ref">Kyrkjebø, R. (2001). _Heimskringla I etter Jofraskinna: Karakteristikk av tekstvitna samt tekstkritisk utgåve av Jens Nilssøns avskrift i AM 37 folio_ /[Doktorgradsavhandling, Universitetet i Bergen].</span>

::: details I EndNote

Legges inn som "Thesis".

Trykt avhandling: Fyll ut feltene Author (trykk "enter" mellom hvert navn hvis flere), Year, Title, University, Thesis Type.

Avhandling på Internett: Fyll ut feltene Author (trykk "enter" mellom hvert navn hvis flere), Year, Title, Thesis Type. University, Place Published (navn på arkiv), URL.

::: 
:::: 

::: details Flere eksempler
<span class="ref"> (Jerpseth, 2017, s. 40) </span>

<span class="ref"> Jerpseth, H. (2017). Older patients with late-stage COPD: Care and clinical decision-making: A qualitative study with perspectives of patients, nurses and physicians [Doktorgradsavhandling, Universitetet i Oslo]. DUO Vitenarkiv. http://urn.nb.no/URN:NBN:no-58501 </span>
:::

### Statistikk og datasett

For statistikker må du redegjøre for hvilke søk du har gjort, og hvordan du oppnådde resultatene etterfulgt av kildehenvisning. Det er anbefalt at man inkluderer en henvisning i teksten og innførsel i referanselista til et datasett når man enten (a) har gjennomført en ny analyse av åpent tilgjengelige data eller (b) har arkivert egne data som presenteres for første gang.

::: teksten
... (Statistisk sentralbyrå, 2017) 

... (Sundstøl, 2018)
:::

:::: ref
<span class="ref">Statistisk sentralbyrå. (2017). _08145: Nye studenter i høyere utdanning i Norge. Kjønn og alder 2000 – 2016_ [Statistikk]. https://www.ssb.no/statbank/table/08145</span>

<span class="ref">Sundstøl, S. A. (2018). _Soils at Haukeli, Imingfjell, and Lesja_ [Datasett].    USN ORA. https://doi.org/10.23642/usn.7454279.v1</span>

::: details Endnote
Legges inn som "Dataset".
Fyll ut feltene: Investigators, Year, Title, Distributor, Datatype, DOI, URL. Note: Sett komma etter navnet på investigators.
:::
::::

### Standarder
Disse dokumenttypene kan ligne bøker og e-bøker og er gjerne utgitt av offentlige eller private institusjoner. Vanligvis har de en seriekode; denne oppgis i parentes etter tittelen. 

::: teksten
(forfatter/organisasjon, publiseringstidspunkt)

... (Institute of Electrical and Electronics Engineers, 1987)
:::

:::: ref
<span class="ref">Institute of Electrical and Electronics Engineers. (1987). _IEEE guide for handling and disposal of transformer grade insulating liquids containing PCBs_ (ANSI/IEEE Std. 799-1987). https://doi.org/10.1109/IEEESTD.1987.81034</span>

::: details Endnote
Legges inn som "Report".

Fyll ut feltene: Author (med et komma etter organisasjonsnavnet eller institusjonsnavnet), Year, Title, Report Number, URL.
:::
::::

::: Flere eksempler

... tidligere utgitt som Retningslinjer for retningslinjer (Statens helsetilsyn, 2002)
... (Helsedirektoratet, 2015)

<span class="ref">Statens helsetilsyn. (2002)._Retningslinjer for retningslinjer: Veileder: Prosesser og metoder for utvikling og implementering av faglig retningslinjer_ (IK-2653). Statens helsetilsyn.</span>
<span class="ref">Helsedirektoratet. (2015). _Veileder om assistert befruktning med donorsæd_ (IS-2418). https://www.shorturl.at/akvB8</span>

### Forelesningsmateriell og intranett-ressurser

Noen publikasjoner er ikke tilgjengelig for alle, noe som påvirker hvordan de refereres til. Det kan være dokumenter fra læringsplattformer som Canvas, eller fra intranett på jobben. Disse verkene er kun tilgjengelig for undervisere og medstudenter eller kollegaer. Dersom dette er dokumenter som bare du har tilgang til, kan du vurdere å ta dem med som vedlegg.  

Om du er ansatt og skriver for kolleger, kan du følge malen for rapporter. Dersom du ikke kan vise til kilden må du referere til dokumentet i løpende tekst som personlig kommunikasjon. Hvis de du skriver for har mulighet til å få tak i arbeidene du bruker, kan du følge malen for PowerPoint-presentasjoner, som vist nedenfor:

::: teksten
(forfatter, årstall, evt. lysark)
...  (Coyle, u.å., lysark 65)
:::

:::: ref 
Forfatter(e). (Publiseringstidspunkt). _Tittel: Undertittel_ /[Lysarkpresentasjon/]. URL
<span class="ref">Coyle, K. (u.å.). _Bibliographic data: A new context_[Lysarkpresentasjon]. Karen Coyle. http://www.kcoyle.net/presentations/oslo2011.pdf</span>

:::details Endnote
Legges innsom "Book"
Fyll ut feltene: Author (trykk "enter" mellom hvert navn hvis flere), Year, Title, Type og Work, Publisher, URL
:::
::::


### Reproduksjon av bilder, figurer og tabeller

Bilder og tabeller som reproduseres skal krediteres i figur- eller tabellteksten, der det også skal opplyses om eventuell rettighetsklarering eller lisensbetingelser, eksempelvis Creative Commons-lisens eller om det er gjengitt med tillatelse. Her er det vertsdokumentet – boken, nettsiden eller artikkelen som bildet er hentet fra – som føres inn i referanselisten. Figurteksten skal innledes med en kort beskrivelse av bildet.

::: tip Bilder du har tatt selv: 
Inkluder beskrivelse, stedsangivelse og dato.  
Figur 1. Utsikt fra Ulriken, Bergen 26. juli 2018.

Ingen innførsel i referanselisten
:::

::: tip Bilde hentet fra nettside:  
Figur 2. Breiflabb. Fra «Monkfish/Breiflabb», av A. Hansen, 2006 ([https://www.flickr.com/photos/xoto/108041783](https://www.flickr.com/photos/xoto/108041783)). CC BY-NC-ND 2.0

Referer til nettsiden:  
<span class="ref">Hansen, A. (2006). _Monkfish / Breiflabb_. Hentet fra [https://www.flickr.com/photos/xoto/108041783](https://www.flickr.com/photos/xoto/108041783)</span>
:::

::: tip Tabell hentet fra tidsskriftartikkel:  
Tabell 1. Sammenligning av mislykkethet. Fra «Unsuccessful treatments of “writer's block”: A meta-analysis,» av D. C. McLean & B. R. Thomas, 2014, _Psychological Reports, 115_, s. 277. Copyright 2014 Sage Publications. Gjengitt med tillatelse.

Referer til artikkelen:  
<span class="ref">McLean, D. C. & Thomas, B. R. (2014). Unsuccessful treatments of “writer's block”: A meta-analysis. _Psychological Reports, 115_(1), 276-278.  [https://doi.org/10.2466/28.PR0.115c12z0](https://doi.org/10.2466/28.PR0.115c12z0)</span>
:::

::: tip Bilde hentet fra bok  
Figur 4. Relativ nettoflytting i kommunene. Fra _Hvorfor vokser steder? Og hvordan kan utviklingen påvirkes?_ (s. 42), av K. Vareide, 2018, Oslo: Cappelen Damm Akademisk. CC BY 4.0.

Referer til boken:  
<span class="ref">Vareide, K. (2018). _Hvorfor vokser steder? Og hvordan kan utviklingen påvirkes?_ [https://doi.org/10.23865/noasp.32](https://doi.org/10.23865/noasp.32)</span>
:::

### Personlig kommunikasjon (e-poster, eget eksamensarbeid, forelesningsnotater o.l.)

Personlig kommunikasjon omfatter ulike typer kilder som ikke er tilgjengelige for andre. Eksempel på kildetyper i denne kategorien er e-poster, tekstmeldinger, uttalelser eller sitater fra chattetjenester, personlige intervjuer, brev, meldinger fra uarkiverte diskusjonsgrupper, forelesningsnotater, telefonsamtaler, tidligere innlevert eksamensarbeid osv. Når det gjelder eksamensarbeid er det viktig at du skriver referanse selv om det er ditt eget arbeid! Bruk personlig kommunikasjon som kilde kun når gjenfinnbare kilder ikke er tilgjengelige. 

::: teksten
... har ofte en fastlagt form (D. Didriksen, personlig kommunikasjon, 17. desember 2017)...
_Ingen innførsel i referanselisten_
:::
